//
//  OTPVerificationViewController.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 02/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

//import UIKit
//import SkyFloatingLabelTextField
//import Alamofire
//import FirebaseAuth
//import IQKeyboardManagerSwift


//class OTPVerificationViewController: UIViewController,UITextFieldDelegate {
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//    }
//
//    static func getInstance() -> OTPVerificationViewController {
//        let storyboard = UIStoryboard(name: "Login",bundle: nil)
//        return storyboard.instantiateViewController(withIdentifier: "OTPVerificationViewController") as! OTPVerificationViewController
//    }
//
//
//    @IBAction func btnAction_openLoginScreen(_ sender : UIButton){
//        self.navigationController?.pushViewController(SignInVC.getInstance(), animated: true)
//    }
//}



import UIKit
import SkyFloatingLabelTextField
import Alamofire
import FirebaseAuth
import IQKeyboardManagerSwift



class OTPVerificationViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet var phoneNumLbl: UILabel!
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet var resendOTPBtn: UIButton!
    @IBOutlet var titleLbl: UILabel!
    
    @IBOutlet var firstTF: SkyFloatingLabelTextField!
    @IBOutlet var secondTF: SkyFloatingLabelTextField!
    @IBOutlet var thirdTF: SkyFloatingLabelTextField!
    @IBOutlet var fourthTF: SkyFloatingLabelTextField!
    @IBOutlet var fifthTF: SkyFloatingLabelTextField!
    @IBOutlet var sixThTF: SkyFloatingLabelTextField!
    
    
    var isFromForgot = Bool()
    var fireVerificationId = String()
    var firebaseToken = String()
    var getMobileNumberStr = String()
    var dialCode = String()
    var VERIFICATIONID = String()
    var otpStr = String()
    var otpDataDict : NSDictionary = [:]
    var user_ID = ""
    
    //    var timer : Timer!
    //    var count = 30
    
    
    //MARK:-  View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationItem.title = "OTP"
//
//        if #available(iOS 11, *) {
//            self.navigationController?.navigationBar.prefersLargeTitles = false
//            self.navigationController?.navigationItem.largeTitleDisplayMode = .never
//        }
//
//
//
//        self.phoneNumLbl.text = String(format: "OTP send at %@-%@.", dialCode,getMobileNumberStr)
//        firstTF.becomeFirstResponder()
//        //        configureUI()
        IQKeyboardManager.shared.enable = false
//        firstTF.layer.backgroundColor = UIColor.red.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.hideActivityIndicator()
        IQKeyboardManager.shared.enable = false
        
        firstTF.delegate = self
        secondTF.delegate = self
        thirdTF.delegate = self
        fourthTF.delegate = self
        fifthTF.delegate = self
        sixThTF.delegate = self
        
        firstTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        secondTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        thirdTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        fourthTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        fifthTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        sixThTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
    }
    
    @IBAction func btnAction_Back(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAction_reSendOtp(_ sender : UIButton) {
        self.view.endEditing(true)
        firstTF.text = ""
        secondTF.text = ""
        thirdTF.text = ""
        fourthTF.text = ""
        fifthTF.text = ""
        sixThTF.text = ""
        
        resendOTP()

    }
    
    static func getInstance() -> OTPVerificationViewController {
        let storyboard = UIStoryboard(name: "Login",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "OTPVerificationViewController") as! OTPVerificationViewController
    }
    
    func moveToHomePage()  {
        self.navigationController?.pushViewController(SelectLocationVC.getInstance(), animated: true)
    }
    
    func moveToChangePassword(userToken : String)  {
        //        let hvc = ChangePasswordVC.getInstance()
        //        hvc.userToken = userToken
        //        self.navigationController?.pushViewController(hvc, animated: true)
    }
    
    
    
    //MARK: -  TextFieldDidChange
    @objc func textFieldDidChange(textField: UITextField) {
        
        let text = textField.text
        
        if  text?.count == 1 {
            
            switch textField {
                
            case firstTF:
                firstTF.layer.backgroundColor = UIColor.appButtonColor().cgColor
                secondTF.becomeFirstResponder()
                
            case secondTF:
                secondTF.layer.backgroundColor = UIColor.appButtonColor().cgColor
                thirdTF.becomeFirstResponder()
                
            case thirdTF:
                thirdTF.layer.backgroundColor = UIColor.appButtonColor().cgColor
                fourthTF.becomeFirstResponder()
                
            case fourthTF:
                fourthTF.layer.backgroundColor = UIColor.appButtonColor().cgColor
                fifthTF.becomeFirstResponder()
                
            case fifthTF:
                fifthTF.layer.backgroundColor = UIColor.appButtonColor().cgColor
                sixThTF.becomeFirstResponder()
                
            case sixThTF:
                sixThTF.layer.backgroundColor = UIColor.appButtonColor().cgColor
                sixThTF.becomeFirstResponder()
                
                hideKeyBoard()
                
                otpStr = String(format: "%@%@%@%@%@%@", firstTF.text ?? "", secondTF.text ?? "", thirdTF.text ?? "", fourthTF.text ?? "", fifthTF.text ?? "", sixThTF.text ?? "")
                print(otpStr)
                
            default:
                break
            }
        }
        
        if  text?.count == 0 {
            
            switch textField {
                
            case firstTF:
                firstTF.layer.backgroundColor = UIColor.gray.cgColor
                firstTF.becomeFirstResponder()
                
            case secondTF:
                secondTF.layer.backgroundColor = UIColor.gray.cgColor
                firstTF.becomeFirstResponder()
                
            case thirdTF:
                thirdTF.layer.backgroundColor = UIColor.gray.cgColor
                secondTF.becomeFirstResponder()
                
            case fourthTF:
                fourthTF.layer.backgroundColor = UIColor.gray.cgColor
                thirdTF.becomeFirstResponder()
                
            case fifthTF:
                fifthTF.layer.backgroundColor = UIColor.gray.cgColor
                fourthTF.becomeFirstResponder()
                
            case sixThTF:
                sixThTF.layer.backgroundColor = UIColor.gray.cgColor
                fifthTF.becomeFirstResponder()
                
            default:
                break
            }
        }
        else {
            
        }
    }
    
    //    func configureUI() {
    //        timer = Timer()
    //        timer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(OTPVerificationViewController.update), userInfo: nil, repeats: true)
    //    }
    //    @objc func update() {
    //
    //        if(count >= 0){
    //            let seconds = String(count % 60)
    //            timerLbl.text =  String(format: "%@ seconds", seconds)
    //            count -= 1
    //
    //        }else{
    //            timer?.invalidate()
    //            timer = nil
    //        }
    //    }
    
    //    deinit {
    //        if self.timer != nil{
    //            timer?.invalidate()
    //        }
    //    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        firstTF.textAlignment = .center
        secondTF.textAlignment = .center
        thirdTF.textAlignment = .center
        fourthTF.textAlignment = .center
        fifthTF.textAlignment = .center
        sixThTF.textAlignment = .center
        
        // submitBtn.buttonCornerRadius(radius: 5)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        firstTF.resignFirstResponder()
        //         timer?.invalidate()
    }
    
    //MARK:-  IBActions
//    @IBAction func onTapResendOtpBtn(_ sender: UIButton) {
//        self.view.endEditing(true)
//        firstTF.text = ""
//        secondTF.text = ""
//        thirdTF.text = ""
//        fourthTF.text = ""
//        fifthTF.text = ""
//        sixThTF.text = ""
//
//        resendOTP()
//    }
    
    @IBAction func onTapYouWantChangeNumBtn(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onTapSubmitBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if firstTF.hasText {
            
            if secondTF.hasText {
                
                if thirdTF.hasText {
                    
                    if fourthTF.hasText {
                        
                        if fifthTF.hasText {
                            
                            if sixThTF.hasText {
                                
                                firebase()
                            }
                            else {
                                
                                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Please enter Otp")
                            }
                        }
                            
                        else {
                            
                            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Please enter Otp")
                        }
                    }
                    else {
                        
                        UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Please enter Otp")
                    }
                }
                else {
                    
                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Please enter Otp")
                }
            }
            else {
                
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Please enter Otp")
            }
        }
        else {
            
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Please enter Otp")
        }
    }
    
    func resendOTP() {
        
        resendOTPBtn.tintColor = UIColor.green
        self.showActivityIndicator()
        PhoneAuthProvider.provider().verifyPhoneNumber(String(format: "%@%@", dialCode,getMobileNumberStr), uiDelegate: nil) { (verificationID, error) in
            
            if (error != nil) {
                
                //                self.hideActivityIndicator()
                self.hideActivityIndicator()
                
                ERROR_MESSAGE = error?.localizedDescription ?? ""
                
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                
                return
                
            }
            else {
                
                self.hideActivityIndicator()
                if verificationID != nil {
                    
                    self.VERIFICATIONID = verificationID ?? ""
                    print("Verification Id From Firebase : \(self.VERIFICATIONID)")
                    
                    self.firstTF.text = ""
                    self.secondTF.text = ""
                    self.thirdTF.text = ""
                    self.fourthTF.text = ""
                    self.fifthTF.text = ""
                    self.sixThTF.text = ""
                    
                    self.otpStr = ""
                    
                }
            }
        }
    }
}


//MARK:- FireBaseToken
extension OTPVerificationViewController {
    
    func firebase(){
        
        self.showActivityIndicator()
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: fireVerificationId,
            verificationCode: otpStr)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            
            if error != nil {
                self.hideActivityIndicator()
                ERROR_MESSAGE = error?.localizedDescription ?? ""
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                
                return
            }
            
            let currentUser = Auth.auth().currentUser
            currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
                if error != nil {
                    
                    self.hideActivityIndicator()
                    
                    ERROR_MESSAGE = error?.localizedDescription ?? ""
                    
                    UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                    
                    return
                }
                
                self.hideActivityIndicator()
                guard let tokenFromFirebase = idToken else { return }
                print("Token from Firebase : \(tokenFromFirebase)")
                self.firebaseToken = tokenFromFirebase
                
                
                self.apiCalling_Verify()
                
            }
        }
    }
}



//MARK:-  Otp VerificationAPI
extension OTPVerificationViewController {
    
    func apiCalling_Verify()  {
        
        let params = [ "userId" : self.user_ID, "token" : AppHelper.getClientToken(),
                       "phone" : self.getMobileNumberStr, "dial_code" : self.dialCode,
                       "firebase_token": self.firebaseToken, "verify":"1" ]
        
        AppHelper.apiCallingForDict(apiName: Vefity_Phone, param: params, viewCont: self, successDict: { (successDict, resultDict) in
            
            let dataDict = successDict
            UserDefaults.standard.set(dataDict, forKey: "LOGIN_RESPONSE")
            UserDefaults.standard.set(true, forKey: "IS_LOGGED_IN")
            UserDefaults.standard.synchronize()
            self.moveToHomePage()
            
        }, fail: { (failError) in
            print("Login Error:\(failError.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            
            if messageID == "157" {
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
            } else if messageID == "129" {
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "verify_email_first", value: "")))
            } else {
                UIAlertController.showAlert(vc: self, title: "", message: message)
            }
            
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
            
        }
        
    }
    
    
}



