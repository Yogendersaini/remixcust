//
//  ForgotPassVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 01/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit
import ADCountryPicker
import CoreTelephony


class ForgotPassVC: UIViewController {
    
    
    @IBOutlet weak var btn_Login : UIButton!
    @IBOutlet weak var btn_Register : UIButton!
    @IBOutlet weak var txt_phone : UITextField!
    
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var lblFlagEmoji: UILabel!
    @IBOutlet weak var imgViewFlag : UIImageView!

    
    let picker = ADCountryPicker(style: .grouped)
    var dailingCode = String()
    var dataDict : [String: Any] = [:]
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppHelper.underLineButton_Formatting(sender: self.btn_Login, title: "Log In")
        
    }
    
    
    static func getInstance() -> ForgotPassVC {
        let storyboard = UIStoryboard(name: "Login",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ForgotPassVC") as! ForgotPassVC
    }
    
    
    @IBAction func btnAction_openLoginScreen(_ sender : UIButton){
        self.navigationController?.pushViewController(SignInVC.getInstance(), animated: true)
    }
    
    @IBAction func btnAction_BackScreen(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAction_SendOTP(_ sender : UIButton){
        self.navigationController?.pushViewController(OTPVerificationViewController.getInstance(), animated: true)
    }
    
    
    @IBAction func btnAction_CountryBtn(_ sender: UIButton) {
            self.picker.delegate = self
            picker.showCallingCodes = true
            picker.pickerTitle = "select_country" //(L102Language.AMLocalizedString(key: "select_country", value: ""))
            //picker.defaultCountryCode = "YT"
            picker.hidesNavigationBarWhenPresentingSearch = false
            self.navigationController?.pushViewController(picker, animated: true)
            
        }
        
        
    }
    extension ForgotPassVC: ADCountryPickerDelegate {

        func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {

            countryLbl.text = String(format: "%@ %@", code,dialCode)

            // countryImg.image = picker.getFlag(countryCode: code)
            dailingCode = dialCode

            print("Dialing Code : \(dailingCode)")
            print("code : \(code)")
            print("Dial code : \(dialCode)")

    //        isCountrySelectedForRegister = true

            self.imgViewFlag.image = self.picker.getFlag(countryCode: code)

            self.navigationController?.popViewController(animated: true)
        }
    }
