//
//  CreateNewPassVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 03/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class CreateNewPassVC: UIViewController {

    @IBOutlet weak var txt_NewPassword : UITextField!
    @IBOutlet weak var txt_ConfPassword : UITextField!
    @IBOutlet weak var imgRound : UIImageView!

    var isSecure = true
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> CreateNewPassVC {
        let storyboard = UIStoryboard(name: "Login",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CreateNewPassVC") as! CreateNewPassVC
    }
    
    @IBAction func btnAction_Back(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAction_showPassword(_ sender : UIButton){
        if isSecure {
            self.txt_NewPassword.isSecureTextEntry = false
            self.txt_ConfPassword.isSecureTextEntry = false
            isSecure = false
            self.imgRound.image = UIImage(named: "radio-on")
        } else {
            self.txt_NewPassword.isSecureTextEntry = true
            self.txt_ConfPassword.isSecureTextEntry = true
            isSecure = true
            self.imgRound.image = UIImage(named: "radio_off")
        }
    }

}
