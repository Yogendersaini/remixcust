//
//  SignInVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 01/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit
import ADCountryPicker
import FirebaseAuth
import CoreTelephony
import IQKeyboardManagerSwift

import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices


class SignInVC: UIViewController, GIDSignInDelegate {
    
    @IBOutlet weak var btn_ShowPassword : UIButton!
    @IBOutlet weak var btn_Login : UIButton!
    @IBOutlet weak var btn_Register : UIButton!
    @IBOutlet weak var btn_forgotPass : UIButton!
    @IBOutlet weak var view_Facebook : UIView!
    @IBOutlet weak var view_Google : UIView!
    @IBOutlet weak var lbl_ShowPass : UILabel!
    @IBOutlet weak var txt_phone : UITextField!
    @IBOutlet weak var txt_password : UITextField!
    @IBOutlet weak var imgRound : UIImageView!
    
    var isSecure = true
    
    
    let picker = ADCountryPicker(style: .grouped)
    var dailingCode = "+91"
    var dataDict : [String: Any] = [:]
    
    
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var lblFlagEmoji: UILabel!
    @IBOutlet weak var imgViewFlag : UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        AppHelper.underLineButton_Formatting(sender: self.btn_Register, title: "Register Now")
        self.setup_Google_Login()
    }
    
    static func getInstance() -> SignInVC {
        let storyboard = UIStoryboard(name: "Login",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
    }
    
    @IBAction func btnAction_openRegisterScreen(_ sender : UIButton){        
        self.navigationController?.pushViewController(SignUpVC.getInstance(), animated: true)
    }

    @IBAction func btnAction_openLocationScreen(_ sender : UIButton){
        self.navigationController?.pushViewController(SelectLocationVC.getInstance(), animated: true)
    }

    
    @IBAction func btnAction_forgotPassScreen(_ sender : UIButton){
        self.navigationController?.pushViewController(ForgotPassVC.getInstance(), animated: true)
    }
    
    @IBAction func btnAction_googleSign(_ sender: UIButton) {
            GIDSignIn.sharedInstance().signIn()
    }

    @IBAction func btnAction_showPassword(_ sender : UIButton){
        if isSecure {
            self.txt_password.isSecureTextEntry = false
            isSecure = false
            self.imgRound.image = UIImage(named: "radio-on")
            self.lbl_ShowPass.text = "Hide Password"
        } else {
            self.txt_password.isSecureTextEntry = true
            isSecure = true
            self.imgRound.image = UIImage(named: "radio_off")
            self.lbl_ShowPass.text = "Show Password"
        }
    }
        
    func setupViews()  {
        countryLbl.text = "\(AppHelper.getCountryCode().1) \(AppHelper.getCountryCode().0)+"
        dailingCode = "+" + AppHelper.getCountryCode().0
        print("Counrty code :- - -- - - - - - - - - - - - - - - ->", AppHelper.getCountryCode())
        self.lblFlagEmoji.text = AppHelper.countryFlag(countryCode: AppHelper.getCountryCode().1)
    }
    
    
    @IBAction func btnAction_CountryBtn(_ sender: UIButton) {
        self.picker.delegate = self
        picker.showCallingCodes = true
        picker.pickerTitle = "select_country" //(L102Language.AMLocalizedString(key: "select_country", value: ""))
        //picker.defaultCountryCode = "YT"
        picker.hidesNavigationBarWhenPresentingSearch = false
        self.navigationController?.pushViewController(picker, animated: true)
    }
    
    
}

extension SignInVC: ADCountryPickerDelegate {

    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {

        countryLbl.text = String(format: "%@ %@", code,dialCode)

        // countryImg.image = picker.getFlag(countryCode: code)
        dailingCode = dialCode

        print("Dialing Code : \(dailingCode)")
        print("code : \(code)")
        print("Dial code : \(dialCode)")

//        isCountrySelectedForRegister = true

        self.imgViewFlag.image = self.picker.getFlag(countryCode: code)

        self.navigationController?.popViewController(animated: true)
    }
}
extension SignInVC {
    // MARK :- API
    
    @IBAction func btnAction_Signin(_ sender : UIButton) {
        
        self.hideKeyBoard()
        
        self.txt_phone.text = self.txt_phone.text?.trimmingCharacters(in: .whitespaces)
        self.txt_password.text = self.txt_password.text?.trimmingCharacters(in: .whitespaces)
                
        if self.txt_phone.hasText {
        } else {
//            self.view.makeToast ((L102Language.AMLocalizedString(key: "please_enter_Phone, value:")), duration: 2.0, position: .bottom, style: style)
            self.view.makeToast ("Please enter phone number")
            return
        }
                
        
        if self.txt_password.hasText {
        } else {
//            self.view.makeToast ((L102Language.AMLocalizedString(key: "Please_Enter_password", value: "")), duration: 2.0, position: .bottom, style: style)
            self.view.makeToast ("Please Enter password")
            return
        }
        self.apiCalling_Login_Manual(phoneNumber: self.txt_phone.text ?? "", password: self.txt_password.text ?? "")

        
    }

    func apiCalling_Login_Manual(phoneNumber: String, password : String){

        let fcmToken = UserDefaults.standard.value(forKey: "FCM_REGISTRATION_TOKEN") as? String ?? ""
        print("FCM from messginig ", fcmToken)
        
        let params = [ "password"  : password,
                       "phone" :   phoneNumber,
                       "dial_code" : dailingCode ]
                
        AppHelper.apiCallingForDict(apiName: Login_With_Phone, param: params, viewCont: self, successDict: { (successDict, resultDict) in
            
            print(successDict)
            
                self.dataDict = successDict
                UserDefaults.standard.set(self.dataDict, forKey: "LOGIN_RESPONSE")
//                DispatchQueue.main.async {
                    UserDefaults.standard.set(true, forKey: IS_LOGGED_NOT_ADDRESS)
                    UserDefaults.standard.synchronize()
//                }

                        self.moveTo_AddressSelection()
                
                /*
                 {
                     message = success;
                     result =     {
                         "activation_code" = 4159;
                         "address_count" = 1;
                         createdOn = "2021-06-03 05:30:29";
                         "dial_code" = "+91";
                         email = "ajay@mailinator.com";
                         "fcm_id" = "fdRZojt8Tjic0jVDtNbM--:APA91bEeBdTNf0cn567VkBU9kbJJ3IDrHGVA-VPsCe2brXlWUFuHOdbjDXD3aqf0BwMOXsEAq11dQMYRClfQiuE8zTW9RjMC9oxOl9ksBviZMU-YFQakGDz3TZPIeVxEBBkvww3em97u";
                         fullName = ajay;
                         "is_approve" = 0;
                         "is_phone_verified" = 1;
                         "is_retailer" = 0;
                         "login_type" = manual;
                         password = 25d55ad283aa400af464c76d713c07ad;
                         phone = 9928791990;
                         photo = "";
                         "req_retailer" = 1;
                         "social_id" = "";
                         token = 32bd802b1612e37d4a57052ac8caa647e9676633;
                         updatedAt = "2021-06-09 07:23:11";
                         userId = 70;
                     };
                     status = 1;
                 }
                 
                  after login open choose address screen
                 
                 */

            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
                UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    func apiLogin_Google_Login(social_id: String, email: String, Name : String, socialApp : String, accessToken : String)  {
        
        let urlString = signUp_SocialLogin
        
        let params = [ "fullName": Name, "email" : email, "phone" :"", "dial_code" : "", "login_type" : "social", "socialApp" : socialApp, "accessToken" : accessToken, "social_id" : social_id ]
        
        
        AppHelper.apiCallingForDict(apiName: urlString, param: params, viewCont: self, successDict: { (successDict, resultDict) in
            self.dataDict = successDict
            UserDefaults.standard.set(self.dataDict, forKey: "LOGIN_RESPONSE")
            DispatchQueue.main.async {
                UserDefaults.standard.set(true, forKey: IS_LOGGED_NOT_ADDRESS)
                UserDefaults.standard.synchronize()
            }
            self.moveTo_AddressSelection()
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
                UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
    }
    
    func moveTo_AddressSelection() {
        let vc = AddressVC.getInstance()
        vc.isForSelection=true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension SignInVC {
    // Google sign in
    
    func setup_Google_Login() {
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().clientID = "1045523608334-ghv33f1oim0gkdij64emmdpd4v16t5qs.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            print(error ?? "google error")
            return
        }
        
        GIDSignIn.sharedInstance()?.signOut()
        
        print(user.profile.email)
        print(user.userID)
        print(user.profile.name)
        
        self.apiLogin_Google_Login(social_id: user.userID, email: user.profile.email, Name: user.profile.name, socialApp: "gmail", accessToken: "")
        
    }
    
    
    
}

