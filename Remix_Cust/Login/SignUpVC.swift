//
//  SignUpVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 01/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit
import ADCountryPicker
import CoreTelephony
import FirebaseAuth
import Alamofire


class SignUpVC: UIViewController {
    
    @IBOutlet weak var btn_ShowPassword : UIButton!
    @IBOutlet weak var btn_Login : UIButton!
    @IBOutlet weak var btn_Register : UIButton!
    @IBOutlet weak var btn_Check : UIButton!
    @IBOutlet weak var btn_Check_Retailer : UIButton!
    @IBOutlet weak var lbl_Terms : UILabel!
    @IBOutlet weak var lbl_ShowPass : UILabel!
    @IBOutlet weak var txt_phone : UITextField!
    @IBOutlet weak var txt_password : UITextField!
    @IBOutlet weak var txt_Name : UITextField!
    @IBOutlet weak var txt_Email : UITextField!
    @IBOutlet weak var imgRound : UIImageView!
    
    var isSecure = true
    var isCheck = false
    var is_Check_Retailer = false
    let picker = ADCountryPicker(style: .grouped)
    var dailingCode = "+91"
    var dataDict : [String: Any] = [:]
    var phoneNumberVerified = String()
    var VerificationID = String()

    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var lblFlagEmoji: UILabel!
    @IBOutlet weak var imgViewFlag : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        AppHelper.underLineButton_Formatting(sender: self.btn_Login, title: "Log In")
        
        self.stupData()
        
    }
    
    
    func stupData() {
    
        self.txt_Name.text = "Ajay"
        self.txt_phone.text = "9928791990"
        self.txt_Email.text = "ajay@mailinator.com"
        self.txt_password.text = "123456"
    }
    
    static func getInstance() -> SignUpVC {
        let storyboard = UIStoryboard(name: "Login",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
    }
    
    
    @IBAction func btnAction_openLoginScreen(_ sender : UIButton){
        self.navigationController?.pushViewController(SignInVC.getInstance(), animated: true)
    }
    
    @IBAction func btnAction_showPassword(_ sender : UIButton){
        if isSecure {
            self.txt_password.isSecureTextEntry = false
            isSecure = false
            self.imgRound.image = UIImage(named: "radio-on")
            self.lbl_ShowPass.text = "Hide Password"

        } else {
            self.txt_password.isSecureTextEntry = true
            isSecure = true
            self.imgRound.image = UIImage(named: "radio_off")
            self.lbl_ShowPass.text = "Show Password"

        }
    }
    
    @IBAction func btnAction_checkNow(_ sender : UIButton){
        if isCheck {
            self.btn_Check.setImage(UIImage(named: "uncheckbox"), for: .normal)
            isCheck = false
        } else {
            self.btn_Check.setImage(UIImage(named: "checkbox"), for: .normal)
            isCheck = true
        }
    }
    
    @IBAction func btnAction_check_Retailer(_ sender : UIButton){
          if is_Check_Retailer {
              self.btn_Check_Retailer.setImage(UIImage(named: "uncheckbox"), for: .normal)
              is_Check_Retailer = false
          } else {
              self.btn_Check_Retailer.setImage(UIImage(named: "checkbox"), for: .normal)
              
              is_Check_Retailer = true
          }
      }

        
    func setupViews()  {
        countryLbl.text = "\(AppHelper.getCountryCode().1) \(AppHelper.getCountryCode().0)+"
        dailingCode = "+" + AppHelper.getCountryCode().0
        print("Counrty code :- - -- - - - - - - - - - - - - - - ->", AppHelper.getCountryCode())
        self.lblFlagEmoji.text = AppHelper.countryFlag(countryCode: AppHelper.getCountryCode().1)
    }
    
    func moveTo_OTPVerification() {
        let otpVC = OTPVerificationViewController.getInstance()
        self.navigationController?.pushViewController(otpVC, animated: true)
    }
    
    
    @IBAction func btnAction_CountryBtn(_ sender: UIButton) {
        self.picker.delegate = self
        picker.showCallingCodes = true
        picker.pickerTitle = "select_country" //(L102Language.AMLocalizedString(key: "select_country", value: ""))
        //picker.defaultCountryCode = "YT"
        picker.hidesNavigationBarWhenPresentingSearch = false
        self.navigationController?.pushViewController(picker, animated: true)
        
    }
    
    @IBAction func btnAction_SignUp(_ sender : UIButton) {
           
           self.hideKeyBoard()
           
           self.txt_Name.text = self.txt_Name.text?.trimmingCharacters(in: .whitespaces)
           self.txt_Email.text = self.txt_Email.text?.trimmingCharacters(in: .whitespaces)
           self.txt_phone.text = self.txt_phone.text?.trimmingCharacters(in: .whitespaces)
           self.txt_password.text = self.txt_password.text?.trimmingCharacters(in: .whitespaces)
           
           if self.txt_Name.hasText {
           } else {
               self.view.makeToast ("Please enter Name")
               return
           }

           if self.txt_phone.hasText {
           } else {
               self.view.makeToast ("Please enter phone number")
               return
           }
                           
           if self.txt_Email.hasText {
               if AppHelper.isValidEmail(with: self.txt_Email.text){
               } else {
                   self.view.makeToast ("Please enter valid email id")
                   return
               }
           } else {
               self.view.makeToast ("Please enter email")
               return
           }

           if self.txt_password.hasText {
           } else {
               self.view.makeToast ("Please Enter password")
               return
           }
           
           if self.txt_password.hasText {
           } else {
               self.view.makeToast ("Please Enter password")
               return
           }

           if self.isCheck {
            self.apiCalling_Registration(email: self.txt_Email.text!, name: self.txt_Name.text!, phone: self.txt_phone.text!, password: self.txt_password.text!)
           } else {
               self.view.makeToast ("Please select terms and conndition.")
               return
           }
                      
       }
       
    
}
extension SignUpVC: ADCountryPickerDelegate {

    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {

        countryLbl.text = String(format: "%@ %@", code,dialCode)

        // countryImg.image = picker.getFlag(countryCode: code)
        dailingCode = dialCode

        print("Dialing Code : \(dailingCode)")
        print("code : \(code)")
        print("Dial code : \(dialCode)")

//        isCountrySelectedForRegister = true

        self.imgViewFlag.image = self.picker.getFlag(countryCode: code)

        self.navigationController?.popViewController(animated: true)
    }
}

extension SignUpVC {
    
    
    func apiCalling_Registration(email: String, name : String, phone : String, password : String){

        if Reachability.isNetworkAvailable(){

            let fcmToken = UserDefaults.standard.value(forKey: "FCM_REGISTRATION_TOKEN") as? String ?? ""
            print("FCM from messginig ", fcmToken)

            let urlString = "\(BaseURL)\(SignUp)"
            
            let param = ["login_type" : "manual", "fullName" : name, "email" : email, "phone" : phone, "dial_code": dailingCode, "password" : password, "req_retailer" : "0"]

            //    Call<SignUpModel> doSignUp(@Field("fullName") String fullname,
            //                               @Field("email") String email,
            //                               @Field("dial_code") String dialCode,
            //                               @Field("phone") String phoneNumber,
            //                               @Field("password") String password,
            //                               @Field("login_type") String loginType,
            //                               @Field("req_retailer") String req_retailer);

            
            #if DEBUG
            print("BEGIN URLSTRING" + urlString)
            print("BEGIN Parameters \(param)")
            #endif
            
            self.showActivityIndicator()
                        
            guard let url = URL(string: urlString) else {
                return
            }
            
            var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
            urlRequest.httpMethod = "POST"
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
            
            
            AF.upload(multipartFormData: { multiPart in
                for (key, value) in param {
                    if let temp = value as? String {
                        multiPart.append(temp.data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? Int {
                        multiPart.append("\(temp)".data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key as String + "[]"
                            if let string = element as? String {
                                multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                                if let num = element as? Int {
                                    let value = "\(num)"
                                    multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
            }, with: urlRequest)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .responseJSON(completionHandler: { data in
                    
                    switch data.result {
                        
                    case .success(_):
                        do {
                            
                            let dictionary = try JSONSerialization.jsonObject(with: data.data!, options: .fragmentsAllowed) as! NSDictionary
                            
                            print("Success!")
                            print(dictionary)
                            
                            let status_code  = dictionary["status"] as? Int ?? 0
                            let result = dictionary["result"] as! [String:Any]
                            
                            if(status_code == 1) {
                                
                                self.dataDict = result
                                UserDefaults.standard.set(self.dataDict, forKey: "LOGIN_RESPONSE")
                                self.mobileNumberAuthenticationWithFirebase()
                                
                                
                            } else {
                                
                                self.hideActivityIndicator()

                                
                            }

                            self.hideActivityIndicator()
                            
                        }
                        catch {
                            // catch error.
                            print("catch error")
                            self.hideActivityIndicator()
                            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "error_updating", value: "")))
                        }
                        
                        break
                        
                    case .failure(_):
                        self.hideActivityIndicator()
                              UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "error_updating", value: "")))
                        break
                        
                    }
                })
        } else {
            UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "no_internet_found", value: "")))
        }
    }
    
    
    func mobileNumberAuthenticationWithFirebase() {
        phoneNumberVerified =  String(format: "%@%@",dailingCode,self.txt_phone.text ?? "")
        print("Register Mobile Number--------->>>>>\(phoneNumberVerified)")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumberVerified, uiDelegate: nil) { (verificationID, error) in
            
            if (error != nil) {
            
                ERROR_MESSAGE = error?.localizedDescription ?? ""
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                return
            }
            else {
            
                if verificationID != nil {
                
                    self.VerificationID = verificationID ?? ""
                    print("Verification Id From Firebase : \(self.VerificationID)")
                    let vc = OTPVerificationViewController.getInstance()
                    vc.fireVerificationId = self.VerificationID
                    vc.getMobileNumberStr = self.dataDict["phone"] as? String ?? ""
                    vc.user_ID = self.dataDict["userId"] as? String ?? ""
                    vc.dialCode = self.dataDict["dial_code"] as? String ?? ""
                    self.navigationController?.pushViewController(vc, animated: true)

                }
            }
        }
    }
}
        
       

