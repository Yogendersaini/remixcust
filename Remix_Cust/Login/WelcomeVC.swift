//
//  WelcomeVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 01/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices

class WelcomeVC: UIViewController, GIDSignInDelegate {
    
    @IBOutlet weak var scrollView : UIScrollView!
    
    @IBOutlet weak var btn_Login : UIButton!
    @IBOutlet weak var btn_Register : UIButton!
    @IBOutlet weak var btn_ContiueGuest : UIButton!
    @IBOutlet weak var view_Facebook : UIView!
    @IBOutlet weak var view_Google : UIView!
    @IBOutlet weak var lbl_Welcome : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentInset = UIEdgeInsets(top: -100, left: 0, bottom: 0, right: 0)
        AppHelper.underLineButton_Formatting(sender: self.btn_Register, title: "Register Now")
        self.setup_Google_Login()
        
        
        if UserDefaults.standard.bool(forKey: IS_LOGGED_IN) {
            DispatchQueue.main.async {
                let hvc = HomeVC.getInstance()
                
                self.navigationController?.pushViewController(hvc, animated: true)
            }
        }
        else if UserDefaults.standard.bool(forKey: IS_LOGGED_NOT_ADDRESS) {
            DispatchQueue.main.async {
                let hvc = AddressVC.getInstance()
                self.navigationController?.pushViewController(hvc, animated: true)
            }
        }
        
    }
    
    
    @IBAction func btnAction_openLoginScreen(_ sender : UIButton){
        self.navigationController?.pushViewController(SignInVC.getInstance(), animated: true)
    }
    
    
    @IBAction func btnAction_openRegisterScreen(_ sender : UIButton){
        self.navigationController?.pushViewController(SignUpVC.getInstance(), animated: true)
    }

    @IBAction func btnAction_Guest(_ sender : UIButton){
        self.navigationController?.pushViewController(SelectLocationVC.getInstance(), animated: true)
    }
    
    @IBAction func btnAction_googleSign(_ sender: UIButton) {
            GIDSignIn.sharedInstance().signIn()
    }
    
    
    func setup_Google_Login() {
    
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().clientID = "1045523608334-ghv33f1oim0gkdij64emmdpd4v16t5qs.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")

    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            print(error ?? "google error")
            return
        }

        GIDSignIn.sharedInstance()?.signOut()

        print(user.profile.email)
        print(user.userID)
        print(user.profile.name)
        
//        self.registerWithGmailAPI(social_id: user.userID, email: user.profile.email, full_name: user.profile.name)
        
    }


    
}
