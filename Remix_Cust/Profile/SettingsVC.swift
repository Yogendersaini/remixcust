//
//  SettingsVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 03/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

  @IBOutlet weak var tbl_Settings : UITableView!
 
    let arrTitles = ["", "Your Orders","Favorite Food","Delivery Address", "Choose Language", "Terms of Services","Privacy policy", "Request to become retailer", "Signout"]
    let arrImages = ["", "shopping-bag-active","fav-icon","loc-icon", "language", "terms-and-conditions","insurance", "market-store (1)",""]

        
//    SIGN OUT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCElls()
    }
    
    
    static func getInstance() -> OrderDetailsVC {
        let stBoard = UIStoryboard(name: "Orders", bundle: nil)
        return stBoard.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
    }
    
    
    @IBAction func btnAction_back(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension SettingsVC : UITableViewDelegate, UITableViewDataSource {
    
    func  registerTableViewCElls() {
        tbl_Settings.register(UINib(nibName: "EditTbCell", bundle: nil), forCellReuseIdentifier: "EditTbCell")
        tbl_Settings.register(UINib(nibName: "ProfileTBCell", bundle: nil), forCellReuseIdentifier: "ProfileTBCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tbl_Settings.dequeueReusableCell(withIdentifier: "EditTbCell") as! EditTbCell
            return cell
        } else {
            let cell = tbl_Settings.dequeueReusableCell(withIdentifier: "ProfileTBCell") as! ProfileTBCell
            cell.setupData(title: self.arrTitles[indexPath.row], imgName: self.arrImages[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row==0{
            self.navigationController?.pushViewController(EditProfileVC.getInstance(), animated: true)
        }
        if indexPath.row==1{
            let vc = OrdersVC.getInstance()
            vc.isFromSettings=true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row==2{
            self.navigationController?.pushViewController(FavouriteFoodVC.getInstance(), animated: true)
        }
        if indexPath.row==3{
        self.navigationController?.pushViewController(AddressVC.getInstance(), animated: true)
        }

    }
    
    
}

