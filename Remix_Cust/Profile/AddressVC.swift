//
//  AddressVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 08/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class AddressVC: UIViewController {

@IBOutlet weak var tbl_Address : UITableView!
 
    var objModelProfile = ProfileModel()
    var isForSelection = false
    var rowSelected = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCElls()        
    }
        
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.apiCalling_Profile()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    static func getInstance() -> AddressVC {
        let stBoard = UIStoryboard(name: "Profile", bundle: nil)
        return stBoard.instantiateViewController(withIdentifier: "AddressVC") as! AddressVC
    }

    
    @IBAction func btnAction_back(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAction_AddAdress(_ sender : UIButton) {
        self.navigationController?.pushViewController(SelectLocationVC.getInstance(), animated: true)
    }
    

}
extension AddressVC : UITableViewDelegate, UITableViewDataSource {
    
    func  registerTableViewCElls() {
        tbl_Address.register(UINib(nibName: "AddressTbCell", bundle: nil), forCellReuseIdentifier: "AddressTbCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objModelProfile.address.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_Address.dequeueReusableCell(withIdentifier: "AddressTbCell") as! AddressTbCell
        if rowSelected == indexPath.row {
            cell.setupAddress(obj: self.objModelProfile.address[indexPath.row], isFromEditAddress: isForSelection, isAddSelected: true)
        } else {
            cell.setupAddress(obj: self.objModelProfile.address[indexPath.row], isFromEditAddress: isForSelection, isAddSelected: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.rowSelected = indexPath.row
        self.tbl_Address.reloadData()
    }
    
    
}
extension AddressVC {
 
    
    func apiCalling_Profile(){

        let params = ["userId" : AppHelper.getUserID(), "token": AppHelper.getUserToken(), "address": "Vishnu colony, market mpark, ludhiyana", "latitude" : "24.5666", "longitude" : "112.3455" ]
                
        AppHelper.apiCallingForDict(apiName: Get_User_Profile, param: params, viewCont: self, successDict: { (successDict, resultDict) in
            
            self.objModelProfile = ProfileModel.init(with: successDict)
            self.tbl_Address.reloadData()
                        
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
                UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
}

