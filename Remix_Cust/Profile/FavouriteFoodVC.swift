//
//  FavouriteFoodVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 08/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class FavouriteFoodVC: UIViewController {

@IBOutlet weak var tbl_food : UITableView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCElls()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    static func getInstance() -> FavouriteFoodVC {
        let stBoard = UIStoryboard(name: "Profile", bundle: nil)
        return stBoard.instantiateViewController(withIdentifier: "FavouriteFoodVC") as! FavouriteFoodVC
    }
    
    
    @IBAction func btnAction_back(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension FavouriteFoodVC : UITableViewDelegate, UITableViewDataSource {
    
    func  registerTableViewCElls() {
        tbl_food.register(UINib(nibName: "AddressTbCell", bundle: nil), forCellReuseIdentifier: "AddressTbCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tbl_food.dequeueReusableCell(withIdentifier: "AddressTbCell") as! AddressTbCell
            return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row==0{
    //    self.navigationController?.pushViewController(EditProfileVC.getInstance(), animated: true)
        }
    }
    
    
}


