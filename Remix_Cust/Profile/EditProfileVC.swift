//
//  EditProfileVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 07/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit
import ADCountryPicker
import CoreTelephony
import IQKeyboardManagerSwift


class EditProfileVC: UIViewController {


    let picker = ADCountryPicker(style: .grouped)
    var dailingCode = String()
    var dataDict : [String: Any] = [:]

    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var lblFlagEmoji: UILabel!
    @IBOutlet weak var imgViewFlag : UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    static func getInstance() -> EditProfileVC {
        let stBoard = UIStoryboard(name: "Profile", bundle: nil)
        return stBoard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
    }
    
    
    @IBAction func btnAction_back(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupViews()  {
        
        countryLbl.text = "\(AppHelper.getCountryCode().1) \(AppHelper.getCountryCode().0)+"
        dailingCode = "+" + AppHelper.getCountryCode().0
        print("Counrty code :- - -- - - - - - - - - - - - - - - ->", AppHelper.getCountryCode())
        self.lblFlagEmoji.text = AppHelper.countryFlag(countryCode: AppHelper.getCountryCode().1)
    }


    @IBAction func btnAction_CountryBtn(_ sender: UIButton) {
        self.picker.delegate = self
        picker.showCallingCodes = true
        picker.pickerTitle = "select_country" //(L102Language.AMLocalizedString(key: "select_country", value: ""))
        //picker.defaultCountryCode = "YT"
        picker.hidesNavigationBarWhenPresentingSearch = false
        self.navigationController?.pushViewController(picker, animated: true)
        
    }
    
}

extension EditProfileVC: ADCountryPickerDelegate {

    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {

        countryLbl.text = String(format: "%@ %@", code,dialCode)

        // countryImg.image = picker.getFlag(countryCode: code)
        dailingCode = dialCode

        print("Dialing Code : \(dailingCode)")
        print("code : \(code)")
        print("Dial code : \(dialCode)")

//        isCountrySelectedForRegister = true

        self.imgViewFlag.image = self.picker.getFlag(countryCode: code)

        self.navigationController?.popViewController(animated: true)
    }
}

