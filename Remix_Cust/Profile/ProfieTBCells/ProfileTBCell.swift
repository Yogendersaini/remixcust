//
//  ProfileTBCell.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 07/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class ProfileTBCell: UITableViewCell {

    @IBOutlet weak var img : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    
    @IBOutlet weak var btnSignOut : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupData(title : String, imgName : String)  {
        self.lblTitle.text = title
        self.img.image = UIImage(named: imgName)
        if title == "Signout" {
            self.btnSignOut.isHidden = false
        }else {
            self.btnSignOut.isHidden = true
        }
    }
    
}
