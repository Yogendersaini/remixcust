//
//  AddressTbCell.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 08/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class AddressTbCell: UITableViewCell {

    @IBOutlet weak var lblAddTpe : UILabel!
    @IBOutlet weak var lblAdd : UILabel!
    @IBOutlet weak var view_selectAdd : UIView!
    @IBOutlet weak var img_selection : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupAddress(obj : AddressModel, isFromEditAddress : Bool, isAddSelected : Bool) {
        self.lblAdd.text = obj.address
        self.lblAddTpe.text = obj.type
        
        if isFromEditAddress {
            self.view_selectAdd.isHidden = true
        } else {

            self.view_selectAdd.isHidden = false
        }
        
        if isAddSelected{
            self.img_selection.image = UIImage(named: "fill_circle")
        }else {
            self.img_selection.image = UIImage(named: "unfill_img")
        }
        
    }
    
}
