//
//  EditTbCell.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 07/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class EditTbCell: UITableViewCell {

    @IBOutlet weak var img : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblEmail : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData() {
        
    }
    
}
