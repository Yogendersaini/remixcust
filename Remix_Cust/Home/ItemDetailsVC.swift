//
//  ItemDetailsVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 05/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class ItemDetailsVC: UIViewController {

    @IBOutlet weak var tbl_ItemDetails : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCElls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.tabBarController?.tabBar.isHidden = false
    }
    @IBAction func btnAction_BackScreen(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    static func getInstance() -> ItemDetailsVC {
        let stBoard = UIStoryboard(name: "Home", bundle: nil)
        return stBoard.instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
    }
    

}
extension ItemDetailsVC : UITableViewDelegate, UITableViewDataSource {
    
    func  registerTableViewCElls() {
        tbl_ItemDetails.register(UINib(nibName: "TbCell_ItemDesc", bundle: nil), forCellReuseIdentifier: "TbCell_ItemDesc")
        tbl_ItemDetails.register(UINib(nibName: "TbCell_itemImgDetails", bundle: nil), forCellReuseIdentifier: "TbCell_itemImgDetails")
        tbl_ItemDetails.register(UINib(nibName: "TblCell_SpecialReq", bundle: nil), forCellReuseIdentifier: "TblCell_SpecialReq")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tbl_ItemDetails.dequeueReusableCell(withIdentifier: "TbCell_itemImgDetails") as! TbCell_itemImgDetails
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tbl_ItemDetails.dequeueReusableCell(withIdentifier: "TbCell_ItemDesc") as! TbCell_ItemDesc
            return cell
        }
        else {
            let cell = tbl_ItemDetails.dequeueReusableCell(withIdentifier: "TblCell_SpecialReq") as! TblCell_SpecialReq
            return cell
            }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
