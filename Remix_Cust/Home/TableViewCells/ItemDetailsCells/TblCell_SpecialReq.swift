//
//  TblCell_SpecialReq.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 05/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class TblCell_SpecialReq: UITableViewCell, UITextViewDelegate {
    
    @IBOutlet weak var lbl_Details : UILabel!
    @IBOutlet weak var lbl_SpeicalReq : UILabel!
    @IBOutlet weak var lbl_WriteHare : UILabel!
    @IBOutlet weak var lbl_Quantity : UILabel!

    @IBOutlet weak var btn_AddtoCart : UIButton!
    @IBOutlet weak var btn_Minus : UIButton!
    @IBOutlet weak var bnt_Plus : UIButton!
    
    @IBOutlet weak var txtView_Details : UITextView!
    
    var quantity = 1
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnAction_Plus( _ sender : UIButton) {
        
        self.quantity = self.quantity + 1
        self.lbl_Quantity.text = "\(self.quantity)"
    }
    
    @IBAction func btnAction_Minus( _ sender : UIButton) {
        if self.quantity > 1 {
            self.quantity = self.quantity - 1
            self.lbl_Quantity.text = "\(self.quantity)"
        }
    }

    
    @IBAction func btnAction_AddToCart( _ sender : UIButton) {
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
           
           if self.txtView_Details.text.isEmpty {
               self.lbl_WriteHare.isHidden = false
           } else {
               self.lbl_WriteHare.isHidden = true
           }
       }

    
}
