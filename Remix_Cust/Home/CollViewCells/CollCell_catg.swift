//
//  CollCell_catg.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 04/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class CollCell_catg: UICollectionViewCell {

    @IBOutlet weak var viewBacg : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func cellSelected(isSelted : Bool)  {
        if isSelted {
            self.viewBacg.layer.backgroundColor = UIColor.appButtonColor().cgColor
        } else {
            self.viewBacg.layer.backgroundColor = UIColor.clear.cgColor
        }
    }

}
