//
//  HomeVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 03/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.style
    }
    var style:UIStatusBarStyle = .default
    
    @IBOutlet weak var viewTopBar : UIView!
    @IBOutlet weak var collViewStore : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLightContent()
        self.register_CollectionViewCell()
    }
    
    func setLightContent()  {
        if self.style == .lightContent {
            self.style = .default
        } else {
            self.style = .lightContent
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    
    static func getInstance() -> HomeVC {
        let storyboard = UIStoryboard(name: "Home",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
    }


}
extension HomeVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func register_CollectionViewCell() {
        self.collViewStore.register(UINib(nibName: "CollCell_Store", bundle: nil), forCellWithReuseIdentifier: "CollCell_Store")
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collViewStore.dequeueReusableCell(withReuseIdentifier: "CollCell_Store", for: indexPath) as! CollCell_Store
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(StoreDetailsVC.getInstance(), animated: true)
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let widthh = (UIScreen.main.bounds.width) - 30
            return CGSize(width: widthh / 2 , height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 5
    }
    
    
    
}

