//
//  StoreDetailsVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 04/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class StoreDetailsVC: UIViewController {
    
    @IBOutlet weak var collViewStore : UICollectionView!
    @IBOutlet weak var collViewCatg : UICollectionView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.style
    }

    var style:UIStatusBarStyle = .default
    var selectedCatgINdex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setLightContent()
        self.register_CollectionViewCell()
    }
    
    func setLightContent()  {
        if self.style == .lightContent {
            self.style = .default
        } else {
            self.style = .lightContent
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    
    
    @IBAction func btnAction_BackScreen(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    

    static func getInstance() -> StoreDetailsVC {
        let stBoard = UIStoryboard(name: "Home", bundle: nil)
        return stBoard.instantiateViewController(withIdentifier: "StoreDetailsVC") as! StoreDetailsVC
    }
    
}
extension StoreDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func register_CollectionViewCell() {
        self.collViewStore.register(UINib(nibName: "CollCell_Item", bundle: nil), forCellWithReuseIdentifier: "CollCell_Item")
        self.collViewCatg.register(UINib(nibName: "CollCell_catg", bundle: nil), forCellWithReuseIdentifier: "CollCell_catg")
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collViewCatg == collectionView {
            let cell = self.collViewCatg.dequeueReusableCell(withReuseIdentifier: "CollCell_catg", for: indexPath) as! CollCell_catg
            if indexPath.row == self.selectedCatgINdex {
                cell.cellSelected(isSelted: true)
            } else {
                cell.cellSelected(isSelted: false)
            }
            return cell
        } else {
            let cell = self.collViewStore.dequeueReusableCell(withReuseIdentifier: "CollCell_Item", for: indexPath) as! CollCell_Item
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collViewCatg == collectionView {
            selectedCatgINdex = indexPath.row
            self.collViewCatg.reloadData()
        } else {
            self.navigationController?.pushViewController(ItemDetailsVC.getInstance(), animated: true)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collViewCatg == collectionView {
            return CGSize(width: 120 , height: 50)
        } else {
            let widthh = (UIScreen.main.bounds.width) - 42
            return CGSize(width: widthh / 2 , height: 180)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collViewCatg == collectionView {
            return 5
        } else {
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    
    
}


