//
//  AppDelegate.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 01/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseAuth
import Firebase
import UserNotifications
import FirebaseMessaging
import GoogleSignIn
import GooglePlaces
import GoogleMaps


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_ID"
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                
        IQKeyboardManager.shared.enable = true
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
      //  Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
                
        
        self.setupFor_UINavigationsBars()
        self.setupFor_CheckForLanguage()
        self.setupFor_GMS_Map_Keys()
        self.setupFor_Paystack_Keys()
        self.setupFor_LoggedIN()


        
        return true
    }
    
        

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }

      // Print full message.
      print(userInfo)

      completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
      func setupFor_GMS_Map_Keys() {
                
          GMSServices.provideAPIKey(MAP_Key)
          GMSPlacesClient.provideAPIKey(MAP_PlacesKey)
      }
      
      func setupFor_Paystack_Keys() {
//          Paystack.setDefaultPublicKey("pk_test_02ff6637b295f8bcdca1d9d77ceaab88e84d3995")
      }
      
      func setupFor_CheckForLanguage()  {
          let currentLang = UserDefaults.standard.value(forKey: "CurrentLanguage")
          if !(currentLang != nil) {
              L102Language.setAppleLAnguageTo(lang: "en")
          } else {
              L102Language.setAppleLAnguageTo(lang: currentLang as! String)
          }
      }
      
      func setupFor_LoggedIN() {
        
          if UserDefaults.standard.bool(forKey: IS_LOGGED_IN) {
              DispatchQueue.main.async {
                let hvc = HomeVC.getInstance()
                  (self.window?.rootViewController as? UINavigationController)?.pushViewController(hvc, animated: true)
              }
          }
          else if UserDefaults.standard.bool(forKey: IS_LOGGED_NOT_ADDRESS) {
            DispatchQueue.main.async {
                let hvc = AddressVC.getInstance()
                (self.window?.rootViewController as? UINavigationController)?.pushViewController(hvc, animated: true)
            }
        }
        
      }
      
      func setupFor_UINavigationsBars() {
           UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
           UINavigationBar.appearance().shadowImage = UIImage()
           UINavigationBar.appearance().isTranslucent = true
           UINavigationBar.appearance().backgroundColor = UIColor.clear
          
          UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor(red: 8/255, green: 127/255, blue: 35/255, alpha: 1)]
           
           UINavigationBar.appearance().largeTitleTextAttributes = [
               NSAttributedString.Key.foregroundColor: UIColor(red: 8/255, green: 127/255, blue: 35/255, alpha: 1)
          ]
                  
          UINavigationBar.appearance().tintColor = UIColor.darkGray //your desired color here
          // UIApplication.shared.statusBarStyle = .lightContent
      }

}
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        UserDefaults.standard.set(fcmToken, forKey: "FCM_REGISTRATION_TOKEN")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
}
//extension AppDelegate : MessagingDelegate {
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
//      print("Firebase registration token: \(String(describing: fcmToken))")
//
//      let dataDict:[String: String] = ["token": fcmToken ?? ""]
//      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
//      // TODO: If necessary send token to application server.
//      // Note: This callback is fired at each app startup and whenever a new token is generated.
//    }
//
//    func application(application: UIApplication,
//                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//      Messaging.messaging().apnsToken = deviceToken
//    }
//}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

    
  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)

    // ...

    // Print full message.
    print(userInfo)

    // Change this to your preferred presentation option
    completionHandler([[.alert, .sound]])
  }

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    let userInfo = response.notification.request.content.userInfo

    // ...

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)

    // Print full message.
    print(userInfo)

    completionHandler()
  }
    
    
}



/*
 Success!
 {
     message = success;
     result =     {
         "activation_code" = 7901;
         "address_count" = 0;
         createdOn = "2021-06-09 13:28:50";
         "dial_code" = "+91";
         email = "ajay@mailinator.com";
         "fcm_id" = "";
         fullName = Ajay;
         "is_approve" = 0;
         "is_phone_verified" = 0;
         "is_retailer" = 0;
         "login_type" = manual;
         password = 25f9e794323b453885f5181f1b624d0b;
         phone = 9928791990;
         photo = "http://intellisensetechnology.host/remix/";
         "req_retailer" = 0;
         "social_id" = "";
         token = 8d14aa4d2dc19fd4a8acfac00760ac69bf1328cd;
         updatedAt = "2021-06-11 08:51:45";
         userId = 82;
     };
     status = 1;
 }
 */
