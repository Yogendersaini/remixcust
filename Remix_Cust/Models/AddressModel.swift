//
//  AddressModel.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 14/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//


import UIKit

class AddressModel: NSObject {

        
    var address   : String = ""
    var addressId   : String = ""
    var createdOn  : String = ""
    var latitude  : String = ""
    var longitude  : String = ""
    var type  : String = ""
    var updatedAt  : String = ""
    var userId  : String = ""
    
    override init() {
        super.init()
    }
    
    
    init(with dict: [String: Any])
    {
        super.init()
        
        address  = dict["address"] as? String ?? ""
        addressId  = dict["addressId"] as? String ?? ""
        createdOn  = dict["createdOn"] as? String ?? ""
        latitude  = dict["latitude"] as? String ?? ""
        longitude  = dict["longitude"] as? String ?? ""
        type  = dict["type"] as? String ?? ""
        updatedAt  = dict["updatedAt"] as? String ?? ""
        userId  = dict["userId"] as? String ?? ""
                
    }
    
    
}

