//
//  ProfileModel.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 14/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//


import UIKit

class ProfileModel: NSObject {
    
    var activation_code  : String = ""
    var  address = [AddressModel]()
    
    var createdOn  : String = ""
    var dial_code   : String = ""
    var email   : String = ""
    var fcm_id  : String = ""
    var fullName   : String = ""
    var is_approve   : String = ""
    var is_phone_verified   : String = ""
    var is_retailer   : String = ""
    var login_type   : String = ""
    var password   : String = ""

    var phone   : String = ""
    var photo   : String = ""
    var req_retailer   : String = ""
    var social_id   : String = ""
    var token   : String = ""
    var updatedAt   : String = ""
    var userId   : String = ""
    
    override init() {
        super.init()
    }
    
    
    init(with dict: [String: Any])
    {
        super.init()

        activation_code = dict["dial_code"] as? String ?? ""
        
        createdOn  = dict["dial_code"] as? String ?? ""
        dial_code   = dict["dial_code"] as? String ?? ""
        email   = dict["dial_code"] as? String ?? ""
        fcm_id = dict["dial_code"] as? String ?? ""
        fullName  = dict["dial_code"] as? String ?? ""
        is_approve = dict["dial_code"] as? String ?? ""
        is_phone_verified  = dict["dial_code"] as? String ?? ""
        is_retailer  = dict["dial_code"] as? String ?? ""
        login_type  = dict["dial_code"] as? String ?? ""
        password   = dict["dial_code"] as? String ?? ""

        phone  = dict["dial_code"] as? String ?? ""
        photo = dict["dial_code"] as? String ?? ""
        req_retailer  = dict["dial_code"] as? String ?? ""
        social_id = dict["dial_code"] as? String ?? ""
        token  = dict["dial_code"] as? String ?? ""
        updatedAt = dict["dial_code"] as? String ?? ""
        userId  = dict["dial_code"] as? String ?? ""

        let add = dict["address"] as! [[String : Any]]
        
        for i in 0...add.count-1{
            let addDict = AddressModel.init(with: add[i])
            self.address.append(addDict)
        }
    }
    
    
}

/*
 "activation_code" = 8681;
 address =         (
                 {
         address = "Civil Lines, NBC Road, Jaipur, 302006, India";
         addressId = 419;
         createdOn = "2021-06-14 10:39:54";
         latitude = "26.9125457";
         longitude = "75.7757102";
         type = home;
         updatedAt = "2021-06-14 10:39:54";
         userId = 89;
     }
 );
 createdOn = "2021-06-14 09:10:58";
 "dial_code" = "+91";
 email = "ajay@mailinator.com";
 "fcm_id" = "eaQJ3VG9QkmoAjPzXDczxM:APA91bHZwXT7mugQYMp1JPwj4Mnw6SV8G3ij-WQu5ML6lN6vPZfB8JhMm84xVmcKL3TniSP4N2IrRGC566lbqPsgCnlCUyutal_1cJPhdGkMyfvRUbTdg2-iCfz3vcSy94bv1qN-dFas";
 fullName = Ajay;
 "is_approve" = 0;
 "is_phone_verified" = 1;
 "is_retailer" = 0;
 "login_type" = manual;
 password = e10adc3949ba59abbe56e057f20f883e;

 phone = 9928791990;
 photo = "http://intellisensetechnology.host/remix/remix_api/";
 "req_retailer" = 0;
 "social_id" = "";
 token = b95af8662de3103f9bcdcb311e3f21318248c08e;
 updatedAt = "2021-06-14 12:04:12";
 userId = 89;
 */
