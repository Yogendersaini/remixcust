//
//  CartView.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 07/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class CartView: UIView {


    @IBOutlet weak var btn_SelectDate : UIButton!
    @IBOutlet weak var btn_SelectTime : UIButton!
    @IBOutlet weak var view_Piker : UIView!
    @IBOutlet weak var view_SelectDateTime : UIView!
    @IBOutlet weak var datePicker : UIDatePicker!
    
    
    var viewCont = CartVC()
    
//        @IBOutlet weak var btnCancelPiker : UIButton!
//        @IBOutlet weak var btnCancelPiker : UIButton!
//        @IBOutlet weak var btnCancelPiker : UIButton!
//        @IBOutlet weak var btnCancelPiker : UIButton!
//        @IBOutlet weak var btnCancelPiker : UIButton!
//        @IBOutlet weak var btnCancelPiker : UIButton!
    
    @IBAction func btnAction_CancelPicker(_ sender : UIButton){
        self.view_Piker.isHidden = true
    }
    @IBAction func btnAction_DonePicker(_ sender : UIButton){
        self.view_Piker.isHidden = true
        self.btn_SelectDate.setTitle("\(self.datePicker.date)", for: .normal)
    }
    @IBAction func btnAction_Update(_ sender : UIButton){
        self.hide_pikcerView()
    }
    
    @IBAction func btnAction_Cancel(_ sender : UIButton){
        self.hide_pikcerView()
    }
    
    @IBAction func btnAction_SelectDate(_ sender : UIButton){
        self.view_Piker.isHidden = false
    }
    @IBAction func btnAction_SelectTime(_ sender : UIButton){
        self.view_Piker.isHidden = false
    }

    @IBAction func btnAction_HIdePickere(_ sender : UIButton){
        self.view_SelectDateTime.isHidden = true
        viewCont.tabBarController?.tabBar.isHidden = false
    }

    func show_pikcerView() {
        self.view_SelectDateTime.isHidden = false
        self.view_Piker.isHidden = true
        viewCont.tabBarController?.tabBar.isHidden = true

    }
    func hide_pikcerView() {
        self.view_SelectDateTime.isHidden = true
        viewCont.tabBarController?.tabBar.isHidden = false
    }

    
}
