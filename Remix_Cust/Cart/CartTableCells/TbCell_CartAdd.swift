//
//  TbCell_CartAdd.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 05/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class TbCell_CartAdd: UITableViewCell {

    var viewCont = CartVC()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnAction_SelectTime(_ sender : UIButton){
        viewCont.view_Cart.show_pikcerView()
    }
    
    @IBAction func btnAction_PickupTime(_ sender : UIButton){
        viewCont.view_Cart.show_pikcerView()
    }
    
}
