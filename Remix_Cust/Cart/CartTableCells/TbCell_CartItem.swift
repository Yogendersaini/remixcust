//
//  TbCell_CartItem.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 05/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class TbCell_CartItem: UITableViewCell {
    
    
    @IBOutlet weak var lbl_Quantity : UILabel!
    
    @IBOutlet weak var btn_AddtoCart : UIButton!
    @IBOutlet weak var btn_Minus : UIButton!
    @IBOutlet weak var bnt_Plus : UIButton!
    
    var quantity = 1
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnAction_Plus( _ sender : UIButton) {
        
        self.quantity = self.quantity + 1
        self.lbl_Quantity.text = "\(self.quantity)"
    }
    
    @IBAction func btnAction_Minus( _ sender : UIButton) {
        if self.quantity > 1 {
            self.quantity = self.quantity - 1
            self.lbl_Quantity.text = "\(self.quantity)"
        }
    }
}


