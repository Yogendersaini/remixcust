//
//  TbCell_Payments.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 05/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class TbCell_Payments: UITableViewCell {

    @IBOutlet weak var img1 : UIImageView!
    @IBOutlet weak var img2 : UIImageView!
    @IBOutlet weak var img3 : UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnAction_payDeliver(_ sender : UIButton) {
        img1.image = UIImage(named: "fill_circle")
        img3.image = UIImage(named: "unfill_img")
        img2.image = UIImage(named: "unfill_img")
    }

    @IBAction func btnAction_payCard(_ sender : UIButton) {
        img1.image = UIImage(named: "unfill_img")
        img3.image = UIImage(named: "unfill_img")
        img2.image = UIImage(named: "fill_circle")
    }

    @IBAction func btnAction_payGiro(_ sender : UIButton) {
        img1.image = UIImage(named: "unfill_img")
        img3.image = UIImage(named: "fill_circle")
        img2.image = UIImage(named: "unfill_img")
    }

}
