//
//  CartVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 03/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class CartVC: UIViewController {

    @IBOutlet weak var tbl_Cart : UITableView!
    @IBOutlet weak var view_Cart : CartView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCElls()
        self.view_Cart.hide_pikcerView()
        view_Cart.viewCont=self
    }
    
}
extension CartVC : UITableViewDelegate, UITableViewDataSource {
    
    func  registerTableViewCElls() {
        
        tbl_Cart.register(UINib(nibName: "TbCell_CartAdd", bundle: nil), forCellReuseIdentifier: "TbCell_CartAdd")
        tbl_Cart.register(UINib(nibName: "TbCell_CartTotel", bundle: nil), forCellReuseIdentifier: "TbCell_CartTotel")
        tbl_Cart.register(UINib(nibName: "TbCell_CartItem", bundle: nil), forCellReuseIdentifier: "TbCell_CartItem")
        tbl_Cart.register(UINib(nibName: "TbCell_Payments", bundle: nil), forCellReuseIdentifier: "TbCell_Payments")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tbl_Cart.dequeueReusableCell(withIdentifier: "TbCell_CartItem") as! TbCell_CartItem
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tbl_Cart.dequeueReusableCell(withIdentifier: "TbCell_CartTotel") as! TbCell_CartTotel
            return cell
        }
        else if indexPath.row == 2 {
            let cell = tbl_Cart.dequeueReusableCell(withIdentifier: "TbCell_CartAdd") as! TbCell_CartAdd
            cell.viewCont=self
            return cell
        }
        else {
            let cell = tbl_Cart.dequeueReusableCell(withIdentifier: "TbCell_Payments") as! TbCell_Payments
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
