//
//  OrderDetailsVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 07/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class OrderDetailsVC: UIViewController {

  @IBOutlet weak var tbl_Orders : UITableView!
 

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCElls()
    }
    
    
    static func getInstance() -> OrderDetailsVC {
        let stBoard = UIStoryboard(name: "Orders", bundle: nil)
        return stBoard.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
    }
    
    
    @IBAction func btnAction_back(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension OrderDetailsVC : UITableViewDelegate, UITableViewDataSource {
    
    func  registerTableViewCElls() {
        tbl_Orders.register(UINib(nibName: "OrderDetailsTbCell", bundle: nil), forCellReuseIdentifier: "OrderDetailsTbCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_Orders.dequeueReusableCell(withIdentifier: "OrderDetailsTbCell") as! OrderDetailsTbCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
