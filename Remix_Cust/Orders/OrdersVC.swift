//
//  OrdersVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 03/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class OrdersVC: UIViewController {

  @IBOutlet weak var tbl_Orders : UITableView!
 
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var btnBack_WithIcon : UIButton!
    var isFromSettings = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCElls()
        if isFromSettings{
            self.btnBack_WithIcon.isHidden = false
            self.btnBack.isHidden = true
        } else {
            self.btnBack_WithIcon.isHidden = true
            self.btnBack.isHidden = false
        }
    }
    
    static func getInstance() -> OrdersVC {
        let stBoard = UIStoryboard(name: "Orders", bundle: nil)
        return stBoard.instantiateViewController(withIdentifier: "OrdersVC") as! OrdersVC
    }
    
    @IBAction func btnAction_back(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    
}
extension OrdersVC : UITableViewDelegate, UITableViewDataSource {
    
    func  registerTableViewCElls() {
        tbl_Orders.register(UINib(nibName: "OrderTbCell", bundle: nil), forCellReuseIdentifier: "OrderTbCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_Orders.dequeueReusableCell(withIdentifier: "OrderTbCell") as! OrderTbCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                self.navigationController?.pushViewController(OrderDetailsVC.getInstance(), animated: true)
        print(self.navigationController)
        print(OrderDetailsVC.getInstance())
        

    }
    
}

