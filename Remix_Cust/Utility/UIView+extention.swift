//
//  UIView+extention.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 01/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func dropShadowT(scale: Bool = true, color : UIColor) {
        backgroundColor = UIColor.white
        layer.shadowColor =  color.cgColor //UIColor.lightGray.cgColor
        layer.shadowOpacity = 5
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 20
        layer.cornerRadius = 15
        }
    
    func addBlurToView() {
        var blurEffect: UIBlurEffect!
        if #available(iOS 13.0, *) {
            blurEffect = UIBlurEffect(style: .dark)
        }else {
            blurEffect = UIBlurEffect(style: .light)
            
        }
        
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.bounds
        blurredEffectView.alpha = 0.5
        blurredEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurredEffectView)
    }
    
    func removeBlurFromView() {
        
        for subview in self.subviews {
            if subview is UIVisualEffectView{
                subview.removeFromSuperview()
            }
        }
    }
    
    
    
    @IBInspectable
    public var cornerRadius: CGFloat
    {
        set (radius) {
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = radius > 0
        }
        
        get {
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat
    {
        set (borderWidth) {
            self.layer.borderWidth = borderWidth
        }
        
        get {
            return self.layer.borderWidth
        }
    }
    
    @IBInspectable
    public var borderColor:UIColor?
    {
        set (color) {
            self.layer.borderColor = color?.cgColor
        }
        
        get {
            if let color = self.layer.borderColor
            {
                return UIColor(cgColor: color)
            } else {
                return nil
            }
        }
    }
    
    
}
extension UIFont {

//    static func robotoLight(size: CGFloat) -> UIFont {
//        let cfURL = Bundle.main.url(forResource: "Roboto-Light", withExtension: "ttf")! as CFURL
//        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
//        return UIFont(name: "Roboto-Light", size: size)!
//    }
//
//    static func robotoRegular(size: CGFloat) -> UIFont {
//        let cfURL = Bundle.main.url(forResource: "Roboto-Regular", withExtension: "ttf")! as CFURL
//        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
//        return UIFont(name: "Roboto-Regular", size: size)!
//    }
//
//    static func robotoMedium(size: CGFloat) -> UIFont {
//        let cfURL = Bundle.main.url(forResource: "Roboto-Medium", withExtension: "ttf")! as CFURL
//        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
//        return UIFont(name: "Roboto-Medium", size: size)!
//    }
//
//    static func robotoBold(size: CGFloat) -> UIFont {
//        let cfURL = Bundle.main.url(forResource: "Roboto-Bold", withExtension: "ttf")! as CFURL
//        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
//        return UIFont(name: "Roboto-Bold", size: size)!
//    }

    static func buttonTitleFont(size: CGFloat) -> UIFont {
        let cfURL = Bundle.main.url(forResource: "Raleway-Medium", withExtension: "ttf")! as CFURL
        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
        return UIFont(name: "Raleway-Medium", size: size)!
    }

}
extension UIColor {
    static func appButtonColor() -> UIColor {    
        return AppHelper.hexStringToUIColor(hex: "#FF9800")
    }
}
extension NSMutableAttributedString {

    func setColor(color: UIColor, forText stringValue: String) {
       let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }

    func setColorAndUndernine(color: UIColor, forText stringValue: String) {
       let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        self.addAttribute(NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue), value: 1, range: range)
        
    }

}

