//
//  SessionExpired.swift
//  TREKK
//
//  Created by harjitsingh on 25/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func userSessionExpired() {
        
        let defaults = UserDefaults.standard
        
        defaults.set(false, forKey: "IS_LOGGED_IN")
        defaults.synchronize()
        
        resetUserDefaults()
        
        UIAlertController.showAlertWithOkAction(self, title: "please re-login", message: "Incorrect user id or token key", buttonTitle: BUTTON_TITLE, buttonAction: {
            
            let appDelegate = UIApplication.shared.delegate
            appDelegate?.window??.rootViewController = UIStoryboard(name: "Splash", bundle: Bundle.main).instantiateInitialViewController()
            
        })
    }
    
            func resetUserDefaults() {
        
                if scooterTimer != nil {
                    scooterTimer?.invalidate()
                    scooterTimer = nil
                    
                }
//        notificationTimer?.invalidate()
//        notificationTimer = nil
//        
        print("BEGIN:Trekk ======> userDefaults successfully removed from the app")
        
        var langStr = String()
        
        langStr = UserDefaults.standard.value(forKey: "CurrentLanguage") as! String
        print("Language Str : \(langStr)")
               
//        DispatchQueue.main.async {
//
//            let instance = InstanceID.instanceID()
//            instance.deleteID { (error) in
//
//                if error != nil {
//
//                    ERROR_MESSAGE = error?.localizedDescription ?? ""
//                    print("Instance ID Error : \(ERROR_MESSAGE)")
//
//                } else {
//
//                    print("FCM TOKEN Deleted Successfully");
//                }
//            }
//        }
        
        let userDefaults = UserDefaults.standard
        let dict = userDefaults.dictionaryRepresentation()
        
        dict.keys.forEach { key in
            
            userDefaults.removeObject(forKey: key)
        }
        
        userDefaults .synchronize()
        
        
        // Re-saving into UserDefaults
        
        userDefaults .set(langStr, forKey: "CurrentLanguage")
        userDefaults .synchronize()
    }
}

