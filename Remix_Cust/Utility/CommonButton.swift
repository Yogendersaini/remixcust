//
//  CommonButton.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 01/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//


import Foundation
import UIKit

class CommonButton: UIButton {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.layer.backgroundColor =  UIColor.appButtonColor().cgColor
        self.titleLabel?.font = UIFont.buttonTitleFont(size: 16)
        self.tintColor = UIColor.black
        
        
    }
    
    
}
