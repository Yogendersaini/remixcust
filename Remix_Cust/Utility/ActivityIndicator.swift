//
//  ActivityIndicator.swift
//  TREKK
//
//  Created by Harjit Singh on 03/10/19.
//  Copyright © 2019 Harjit Singh. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

extension UIViewController {
    
    //MARK: -  Activity Indicator Show / Hide

    func showActivityIndicator() {
                
        DispatchQueue.main.async {
            
            SVProgressHUD.setDefaultMaskType(.black)
            //SVProgressHUD.setForegroundColor(.darkBlue)
            SVProgressHUD.show(withStatus: (L102Language.AMLocalizedString(key: "please_wait", value: "")))
        }
    }
    
    func hideActivityIndicator() {
        
        DispatchQueue.main.async {
            
            SVProgressHUD.dismiss()
        }
    }
    
    
    //MARK: -  Keyboard Show / Hide

    func showKeyBoard() {
        
        view .endEditing(false)
    }
    
    func hideKeyBoard() {
        
        view .endEditing(true)
    }
    
    
    //MARK: -  Empty Back Button Title

    open override func awakeFromNib() {
            
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.black

    }

}
