//
//  AppHelper.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 02/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import Alamofire
import CoreTelephony


class AppHelper {
    
    
    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func underLineButton_Formatting( sender : UIButton, title : String) {
        let stringValue = title
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
        attributedString.setColorAndUndernine(color: UIColor.appButtonColor(), forText: stringValue)
        sender.setAttributedTitle(attributedString, for: .normal)
    }
    
    
    class func isValidEmail(with email: String?) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailPredicate.evaluate(with: email)
    }
    
    class func validatePassword(_ password: String?) -> Bool {
        
        if password!.count < 8 {
            return false
        }
        if password?.range(of: #"\d+"#, options: .regularExpression) == nil {
            return false
        }
        if password?.range(of: #"\p{Alphabetic}+"#, options: .regularExpression) == nil {
            return false
        }
        if password?.range(of: #"\s+"#, options: .regularExpression) != nil {
            return false
        }
        return true
    }
    
    class func getUserToken() -> String {
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            let dt = data as! [String: Any]
            return dt["token"] as? String ?? ""
        } else {
            return ""
        }
    }

    class func getUserID() -> String {
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            let dt = data as! [String: Any]
             return dt["userId"] as? String ?? ""
        } else {
            return "0"
        }
    }

    class func getClientToken() -> String  {
           if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
               let dt = data as! [String: Any]
                return dt["token"] as? String ?? ""
           } else {
               return "No token"
           }

    }

    class func getUserEmail() -> String {
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            let dt = data as! [String: Any]
             return dt["email"] as? String ?? ""
        } else {
            return "No email"
        }
    }

    class func getUserInviteCode() -> String {
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            let dt = data as! [String: Any]
             return dt["referral_code"] as? String ?? ""
        } else {
            return "0"
        }
    }

    class func getRide_Id() -> String {
        if let data = UserDefaults.standard.value(forKey: "RIDE_RESPONSE") {
            let dt = data as! [String: Any]
             return dt["id"] as? String ?? ""
        } else {
            return "0"
        }
    }

    
    class func apiCallingForArray(apiName : String, param : [String: Any], viewCont : UIViewController, successArray: @escaping([[String : Any]], _ resultDict : NSDictionary) -> Void, fail: @escaping (Error) -> Void, messageID: @escaping (String, String) -> Void, alertMsg: @escaping (String) -> Void) {


        if Reachability.isNetworkAvailable(){

                let urlString = "\(BaseURL)\(apiName)"
                let params = param

                #if DEBUG
                print("BEGIN  ======>URLSTRING" + urlString)
                print("BEGIN  =====Parameters>\(params)")
                #endif
                viewCont.showActivityIndicator()


                guard let url = URL(string: urlString) else {
                    return
                }

                var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
                urlRequest.httpMethod = "POST"
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")


                AF.upload(multipartFormData: { multiPart in
                    for (key, value) in param {
                        if let temp = value as? String {
                            multiPart.append(temp.data(using: .utf8)!, withName: key as String)
                        }
                        if let temp = value as? Int {
                            multiPart.append("\(temp)".data(using: .utf8)!, withName: key as String)
                        }
                        if let temp = value as? NSArray {
                            temp.forEach({ element in
                                let keyObj = key as String + "[]"
                                if let string = element as? String {
                                    multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                                } else
                                    if let num = element as? Int {
                                        let value = "\(num)"
                                        multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                                }
                            })
                        }
                    }
                }, with: urlRequest)
                    .uploadProgress(queue: .main, closure: { progress in
                        //Current upload progress of file
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    .responseJSON(completionHandler: { data in

                        switch data.result {

                        case .success(_):
                            do {

                                let dictionary = try JSONSerialization.jsonObject(with: data.data!, options: .fragmentsAllowed) as! NSDictionary

                                print("Success!")
                                print(dictionary)
                                let json = dictionary
                                let status_code : Int = json["status"] as? Int ?? 0

                                if(status_code == 1) {

                                    if let data = json as? [String : Any] {
                                        successArray(data["result"] as! [[String : Any]], json)
                                    }
                                } else {

                                    viewCont.hideActivityIndicator()
                                    messageID(json["message_id"] as? String ?? "", json["message"] as? String ?? "")
                                }
                                viewCont.hideActivityIndicator()

                            }
                            catch {
                                // catch error.
                                print("catch error")
                                viewCont.hideActivityIndicator()
                                alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                            }
                            break

                        case .failure(_):
                            viewCont.hideActivityIndicator()
                            alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                            break

                        }


                    })
        } else {
            alertMsg((L102Language.AMLocalizedString(key: "no_internet_found", value: "")))
        }
    }



    class func apiCallingForDict(apiName : String, param : [String: Any], viewCont : UIViewController, successDict: @escaping([String : Any], _ resultDict : NSDictionary) -> Void, fail: @escaping (Error) -> Void, messageID: @escaping (String, String) -> Void, alertMsg: @escaping (String) -> Void) {

        if Reachability.isNetworkAvailable(){

            let urlString = "\(BaseURL)\(apiName)"
            let params = param

            #if DEBUG
            print("BEGIN ======>URLSTRING" + urlString)
            print("BEGIN =====Parameters>\(params)")
            #endif
            viewCont.showActivityIndicator()


            guard let url = URL(string: urlString) else {
                return
            }

            var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
            urlRequest.httpMethod = "POST"
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")


            AF.upload(multipartFormData: { multiPart in
                for (key, value) in param {
                    if let temp = value as? String {
                        multiPart.append(temp.data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? Int {
                        multiPart.append("\(temp)".data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key as String + "[]"
                            if let string = element as? String {
                                multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                                if let num = element as? Int {
                                    let value = "\(num)"
                                    multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
            }, with: urlRequest)
//                .uploadProgress(queue: .main, closure: { progress in
//                    //Current upload progress of file
//                    print("Upload Progress: \(progress.fractionCompleted)")
//                })
                .responseJSON(completionHandler: { data in
                    
                    switch data.result {

                    case .success(_):
                        do {

                            let dictionary = try JSONSerialization.jsonObject(with: data.data!, options: .fragmentsAllowed) as! NSDictionary

                            print("Success!")
                            print(dictionary)
                            let json = dictionary // as! NSDictionary

                            let status_code : Int = json["status"] as? Int ?? 0 as? Int ?? 0

                            if(status_code == 1) {

                                if let data = json as? [String : Any] {
                                    successDict((data["result"] as! NSDictionary) as! [String : Any], json)
                                }
                            } else {

                                viewCont.hideActivityIndicator()
                                let msg = json["message"] as? String ?? ""
                                if msg == "Token key not validate" {

                                    UserDefaults.standard.set(false, forKey: "IS_LOGGED_IN")
                                    UserDefaults.standard.synchronize()
                                    let vc = UIViewController()
                                    vc.resetUserDefaults()
                                    let appDelegate = UIApplication.shared.delegate
                                    appDelegate?.window??.rootViewController = UIStoryboard(name: "Login", bundle: Bundle.main).instantiateInitialViewController()


                                } else {
                                    messageID(json["message_id"] as? String ?? "", json["message"] as? String ?? "")
                                }
                            }
                            viewCont.hideActivityIndicator()

                        }
                        catch {
                            // catch error.
                            print("catch error")
                            viewCont.hideActivityIndicator()
                            alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                        }
                        break

                    case .failure(_):
                        viewCont.hideActivityIndicator()
                        alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                        break

                    }


                })


        } else {
            alertMsg((L102Language.AMLocalizedString(key: "no_internet_found", value: "")))
        }
    }



    class func apiCallingWithImage(apiName : String, param : [String: Any], viewCont : UIViewController, imgData : NSData,  successDict: @escaping([String : Any]) -> Void, fail: @escaping (Error) -> Void, messageID: @escaping (String, String) -> Void, alertMsg: @escaping (String) -> Void) {

        if Reachability.isNetworkAvailable(){

            let urlString = "\(BaseURL)\(apiName)"
            let params = param


            #if DEBUG

            print("BEGIN  ======>URLSTRING" + urlString)
            print("BEGIN  =====Parameters>\(params)")

            #endif

            viewCont.showActivityIndicator()



            guard let url = URL(string: urlString) else {
                return
            }

            var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
            urlRequest.httpMethod = "POST"
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")


            AF.upload(multipartFormData: { multiPart in
                for (key, value) in param {
                    if let temp = value as? String {
                        multiPart.append(temp.data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? Int {
                        multiPart.append("\(temp)".data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key as String + "[]"
                            if let string = element as? String {
                                multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                                if let num = element as? Int {
                                    let value = "\(num)"
                                    multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
                if imgData.count > 0 {
                    let data : Data = imgData as Data
                    multiPart.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
                }

            }, with: urlRequest)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .responseJSON(completionHandler: { data in

                    switch data.result {

                    case .success(_):
                        do {

                            let dictionary = try JSONSerialization.jsonObject(with: data.data!, options: .fragmentsAllowed) as! NSDictionary

                            print("Success!")
                            print(dictionary)
                            let json = dictionary // as! NSDictionary

                            let status_code : Int = json["status"] as? Int ?? 0 

                            if(status_code == 1) {

                                if let data = json as? [String : Any] {
                                    successDict((data["result"] as! NSDictionary) as! [String : Any])
                                }
                            } else {

                                viewCont.hideActivityIndicator()
                                messageID(json["message_id"] as? String ?? "", json["message"] as? String ?? "")
                            }
                            viewCont.hideActivityIndicator()

                        }
                        catch {
                            // catch error.
                            print("catch error")
                            viewCont.hideActivityIndicator()
                            alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                        }
                        break

                    case .failure(_):
                        viewCont.hideActivityIndicator()
                        alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                        break

                    }


                })

        } else {
            alertMsg((L102Language.AMLocalizedString(key: "no_internet_found", value: "")))
        }
    }




    class func apiCallingForDict_WithoutLoader(apiName : String, param : [String: Any], viewCont : UIViewController, jsonResponce: @escaping([String : Any]) -> Void, fail: @escaping (Error) -> Void, messageID: @escaping (String, String, NSDictionary) -> Void, alertMsg: @escaping (String) -> Void) {

        if Reachability.isNetworkAvailable(){

            let urlString = "\(BaseURL)\(apiName)"
            let params = param

            #if DEBUG
            print("BEGIN  ======>URLSTRING" + urlString)
            print("BEGIN  =====Parameters>\(params)")
            #endif



            guard let url = URL(string: urlString) else {
                return
            }

            var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
            urlRequest.httpMethod = "POST"
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")


            AF.upload(multipartFormData: { multiPart in
                for (key, value) in param {
                    if let temp = value as? String {
                        multiPart.append(temp.data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? Int {
                        multiPart.append("\(temp)".data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key as String + "[]"
                            if let string = element as? String {
                                multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                                if let num = element as? Int {
                                    let value = "\(num)"
                                    multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
            }, with: urlRequest)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .responseJSON(completionHandler: { data in

                    switch data.result {

                    case .success(_):
                        do {

                            let dictionary = try JSONSerialization.jsonObject(with: data.data!, options: .fragmentsAllowed) as! NSDictionary

                            print("Success!")
                            print(dictionary)
                            let json = dictionary // as! NSDictionary

                            let status_code : Int = json["status"] as? Int ?? 0

                            if(status_code == 1) {
                            //    print(json)
                                if let data = json as? [String : Any] {
                                    jsonResponce(data)
//                                    if let dataDict = json["result"] as? [String : Any] {
//                                        successDict((data["result"] as! NSDictionary) as! [String : Any], json)
//                                    }

                                }
                            } else {

                           //     print(json)

//                                viewCont.hideActivityIndicator()
                                messageID(json["message_id"] as? String ?? "", json["message"] as? String ?? "", json)
                            }
//                            viewCont.hideActivityIndicator()

                        }
                        catch {
                            // catch error.
                            print("catch error")
//                            viewCont.hideActivityIndicator()
                            alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                        }
                        break

                    case .failure(_):
//                        viewCont.hideActivityIndicator()
                        alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                        break

                    }


                })


        } else {
            alertMsg((L102Language.AMLocalizedString(key: "no_internet_found", value: "")))
        }
    }





    class func apiCallingWithSignUp(apiName : String, param : [String: Any], viewCont : UIViewController, dl_image : NSData, ins_image : NSData, rw_image : NSData,  successDict: @escaping([String : Any]) -> Void, fail: @escaping (Error) -> Void, messageID: @escaping (String, String) -> Void, alertMsg: @escaping (String) -> Void) {

        if Reachability.isNetworkAvailable(){

            let urlString = "\(BaseURL)\(apiName)"
            let params = param


            #if DEBUG

            print("BEGIN  ======>URLSTRING" + urlString)
            print("BEGIN  =====Parameters>\(params)")

            #endif

            viewCont.showActivityIndicator()



            guard let url = URL(string: urlString) else {
                return
            }

            var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
            urlRequest.httpMethod = "POST"
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")


            AF.upload(multipartFormData: { multiPart in
                for (key, value) in param {
                    if let temp = value as? String {
                        multiPart.append(temp.data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? Int {
                        multiPart.append("\(temp)".data(using: .utf8)!, withName: key as String)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key as String + "[]"
                            if let string = element as? String {
                                multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                                if let num = element as? Int {
                                    let value = "\(num)"
                                    multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }

                if dl_image.count > 0 {
                    let dl_imageData : Data = dl_image as Data
                    multiPart.append(dl_imageData, withName: "dl_image", fileName: "image.png", mimeType: "image/png")

                    let ins_imageData : Data = dl_image as Data
                    multiPart.append(ins_imageData, withName: "ins_image", fileName: "image.png", mimeType: "image/png")

                    let rw_imageData : Data = dl_image as Data
                    multiPart.append(rw_imageData, withName: "rw_image", fileName: "image.png", mimeType: "image/png")
                }


            }, with: urlRequest)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .responseJSON(completionHandler: { data in

                    switch data.result {

                    case .success(_):
                        do {

                            let dictionary = try JSONSerialization.jsonObject(with: data.data!, options: .fragmentsAllowed) as! NSDictionary

                            print("Success!")
                            print(dictionary)
                            let json = dictionary // as! NSDictionary

                            let status_code : Int = json["status"] as? Int ?? 0

                            if (status_code == 1) {

                                if let data = json as? [String : Any] {
                                    successDict((data as! NSDictionary) as! [String : Any])
                                }
                            } else {

                                viewCont.hideActivityIndicator()
                                messageID(json["message_id"] as? String ?? "", json["message"] as? String ?? "")
                            }
                            viewCont.hideActivityIndicator()

                        }
                        catch {
                            // catch error.
                            print("catch error")
                            viewCont.hideActivityIndicator()
                            alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                        }
                        break

                    case .failure(_):
                        viewCont.hideActivityIndicator()
                        alertMsg((L102Language.AMLocalizedString(key: "error_updating", value: "")))

                        break

                    }


                })

        } else {
            alertMsg((L102Language.AMLocalizedString(key: "no_internet_found", value: "")))
        }
    }

    
    class func getCountryCode() -> (String , String){
        guard let carrier = CTTelephonyNetworkInfo().subscriberCellularProvider, let countryCode = carrier.isoCountryCode else { return ("", "") }
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = prefixCodes[countryCode.uppercased()] ?? ""
        return (countryDialingCode , countryCode.uppercased())
    }
    
        
    class func countryFlag(countryCode: String) -> String {
        let base = 127397
        var tempScalarView = String.UnicodeScalarView()
        for i in countryCode.utf16 {
          if let scalar = UnicodeScalar(base + Int(i)) {
            tempScalarView.append(scalar)
          }
        }
        return String(tempScalarView)
      }

    
    
    
    
    
    
    

}
