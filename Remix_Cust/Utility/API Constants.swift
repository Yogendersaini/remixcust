//
//  API Constants.swift
//  Trekk
//
//  Created by Harjit Singh Mac on 30/03/20.
//  Copyright © 2020 Harjit Singh Mac. All rights reserved.
//

//var PROFILE_DATA_GLOBEL = DashboardDataModel()

import Foundation



//    let BaseURL = "http://intellisensetechnology.host/remix/remix_api/"   // live

    let BaseURL = "http://intellisensetechnology.host/remix/remix_api/api/" // test

//let MAP_Key = "AIzaSyD8cTDcRTYh9Guc_-jWrLY82uFhT5yRBNg"
//let MAP_DirectionKey = "AIzaSyCqJwEohRec-Vnl-AwhkgC_DvbmuHs8ywQ"
//let MAP_PlacesKey = "AIzaSyBY3gJ0r0dxesSL8V6jwRhhU1uRuCgrXTQ"

let MAP_Key = "AIzaSyB3MjDmzU8-izl9DZ2R6bESA86V7k8lmkA"
let MAP_DirectionKey = "AIzaSyB3MjDmzU8-izl9DZ2R6bESA86V7k8lmkA"
let MAP_PlacesKey = "AIzaSyB3MjDmzU8-izl9DZ2R6bESA86V7k8lmkA"


let IS_LOGGED_IN = "IS_LOGGED_IN"  // login and address is choosed so move to home page
let IS_LOGGED_NOT_ADDRESS = "IS_LOGGED_NOT_ADDRESS" // LOGIN is done but address is not setup


let Charge_Customer = "payment/charge_customer"
//    Call<PaymentResponse> orderOnlinePayment(@Field("userId") String userId,
//                                        @Field("token") String token,
//                                        @Field("payment_id") String payment_id,
//                                        @Field("orderId") String orderId);

let Charge_Customer_Two = "payment/charge_customer"
//    Call<Result> ordergiropayPayment(@Field("userId") String userId,
//                                     @Field("token") String token,
//                                     @Field("payment_id") String payment_id,
//                                     @Field("orderId") String orderId,
//                                     @Field("payment_method_type") String payment_method_type);
    

    let Remove_Card = "payment/remove_card"
//    Call<PaymentResponse> removeCard(@Field("userId") String userId,
//                                        @Field("token") String token,
//                                        @Field("payment_id") String payment_id);


    let Get_Cards = "payment/get_cards"
//    Call<PaymentResponse> getCards(@Field("userId") String userId,
//                                   @Field("token") String token);
    
    let Request_For_Retailer = "user/request_for_retailer"
//    Call<ProfileResponse> reqRetail(@Field("userId") String userId,
//                                   @Field("token") String token,
//                                    @Field("type") String type);


    let View_Transcation = "payment/view_transaction"
//    Call<PaymentResponse> checkPayment(@Field("userId") String userId,
//                                   @Field("token") String token,
//                                   @Field("orderId") String orderId,
//                                   @Field("payment_intent_id") String payment_intent_id);

    
    let Create_Customer = "payment/create_customer"
//    Call<PaymentResponse> addCard(@Field("userId") String userId,
//                                  @Field("token") String token,
//                                  @Field("card_token") String card_token,
//                                  @Field("last4") String last4,
//                                  @Field("exp_month") String exp_month,
//                                  @Field("exp_year") String exp_year,
//                                  @Field("card_type") String card_type,
//                                  @Field("card_holder_name") String card_holder_name);

    
    let SignUp = "user/signup"
    //Call<SignUpModel> doSignUp(@Field("fullName") String fullname,
//                               @Field("email") String email,
//                               @Field("dial_code") String dialCode,
//                               @Field("phone") String phoneNumber,
//                               @Field("password") String password,
//                               @Field("login_type") String loginType,
//                               @Field("req_retailer") String req_retailer);

    let signUp_SocialLogin = "user/signup"
//    Call<SignUpModel> doSocialSignUp(@Field("fullName") String fullname,
//                               @Field("email") String email,
//                               @Field("socialApp") String socialApp,
//                               @Field("accessToken") String accessToken,
//                               @Field("dial_code") String dialCode,
//                               @Field("phone") String phoneNumber,
//                               @Field("social_id") String social_id,
//                               @Field("login_type") String login_type,
//                               @Field("password") String password);


    let Login_With_Phone = "user/login_with_phone"
//    Call<SignUpModel> doLoginWithPhone(@Field("dial_code") String dialCode,
//                                       @Field("phone") String phoneNumber,
//                                       @Field("password") String password);


    let Get_Notifications = "user/get_notifications"
//    Call<NotificationResponse> fetchNotification(@Field("userId") String userId,
//                                                 @Field("token") String token);


    let Add_Address = "user/add_address"
//    Call<LocationResponse> setDeliveryLocation(@Field("userId") String userId,
//                                               @Field("address") String address,
//                                               @Field("type") String type,
//                                               @Field("lat") String lat,
//                                               @Field("long") String lng,
//                                               @Field("token") String token);


    let Update_Address = "user/update_address"
//    Call<LocationResponse> updateLocation(@Field("userId") String userId,
//                                          @Field("address") String address,
//                                          @Field("type") String type,
//                                          @Field("lat") String lat,
//                                          @Field("long") String lng,
//                                          @Field("addressId") String addressId,
//                                          @Field("token") String token);



    let DashBoard = "home/dashboard"
//    Call<StoreResponse> getStoreData(@Field("userId") String userId,
//                                     @Field("token") String token,
//                                     @Field("latitude") String latitude,
//                                     @Field("longitude") String longitude,
//                                     @Field("page") String page,
//                                     @Field("limit") String limit);


    let Search = "home/search"
//    Call<StoreResponse> getSearchData(@Field("userId") String userId,
//                                      @Field("token") String token,
//                                      @Field("addressId") String addressId,
//                                      @Field("latitude") String latitude,
//                                      @Field("longitude") String longitude,
//                                      @Field("categoryId") String categoryId,
//                                      @Field("filter") String filter,
//                                      @Field("page") String page,
//                                      @Field("limit") String limit);


    
    let Get_Item_By_Store_name = "item/get_items_by_store_new"
//    Call<ItemModel> getCatSubCat(@Field("storeId") String storeId);


    let GetI_tem_By_Category = "item/get_items_by_category"
//    Call<ItemResult> getItemswithCat(@Field("userId") String userId,@Field("parentCatId") String parentCatId,
//                                     @Field("subCatId") String subCatId,
//                                     @Field("storeId") String storeId);


    let Get_Item_By_ID = "item/get_item_by_id"
//    Call<ProductDetailModel> getItemDetails(@Field("userId") String userId,
//                                            @Field("token") String token,
//                                            @Field("itemId") String itemId);



    let Get_User_Profile = "user/get_user_profile"
//    Call<ProfileResponse> getProfile(@Field("userId") String userId,
//                                     @Field("token") String token, @Field("type") String type,
//                                     @Field("address") String address, @Field("latitude") String latitude,
//                                     @Field("longitude") String longitude);


    let Edit_User_Profile = "user/edit_user_profile"
//    Call<SignUpModel> setProfile(@Part("userId") RequestBody userId,
//                                 @Part("token") RequestBody token,
//                                 @Part("phone") RequestBody phone,
//                                 @Part("fullName") RequestBody fullName,
//                                 @Part("dial_code") RequestBody dial_code,
//                                 @Part MultipartBody.Part profilePic);


    
    let Delete_Address = "user/delete_address"
//    Call<ProfileResponse> deleteAddress(@Field("userId") String userId,
//                                        @Field("addressId") String addressId,
//                                        @Field("token") String token);

//not in doc

    let Vefity_Phone = "user/verify_phone"
//    Call<ProfileResponse> verifyPhoneNumber(@Field("userId") String userId,
//                                            @Field("dial_code") String dialCode,
//                                            @Field("phone") String phone,
//                                            @Field("firebase_token") String firebaseToken);



    let Place_Order = "order/place_order"
//    Call<PlaceOrderResponse> placeOrder(@Field("userId") String userId,
//                                        @Field("token") String token,
//                                        @Field("cart") String cart,@Field("app_version") String app_version,
//                                        @Field("device_info") String device_info);



    let Get_Current_Order = "order/get_current_orders"
//    Call<OrdersResponse> getOrders(@Field("userId") String userId,
//                                   @Field("token") String token,
//                                   @Field("page") String page,
//                                   @Field("limit") String limit);


    let Setup_Intent  = "payment/setupIntent"
//    Call<SetuIntentResponse> getSetupIntentResponse(@Field("userId") String userId,
//                                                    @Field("token") String token);


    let Get_Order_By_ID = "order/get_order_by_id"
//    Call<OrderDetailResponse> getOrderDetails(@Field("userId") String userId,
//                                              @Field("token") String token,
//                                              @Field("orderId") String orderId);

    let Update_Fcm_ID = "user/update_fcm_id"
//    Call<ProfileResponse> sendTokenToServer(@Field("userId") String userId,
//                                            @Field("token") String token,
//                                            @Field("fcm_id") String fcmToken);


    let Forget_Password = "user/forget_password"
//    Call<SignUpModel> forgotPass(@Field("dial_code") String dialCode,
//                                 @Field("phone") String phone);

    let Apply_Promo = "order/apply_promo"
//    Call<PromoCodeResponse> findPromo(@Field("userId") String dialCode,
//                                      @Field("promo_code") String phone);



    let Change_Password = "user/change_password"
//    Call<SignUpModel> changePassword(@Field("userId") String userId,
//                                     @Field("token") String token,
//                                     @Field("new_password") String newPassword,
//                                     @Field("current_password") String current_password);

    let Change_Password_With_Phone = "user/change_password_with_phone"
//    Call<SignUpModel> setPassword(@Field("phone") String phone,
//                                     @Field("dial_code") String dial_code,
//                                     @Field("new_password") String newPassword,
//                                     @Field("firebase_token") String firebase_token);



    let Get_Delivery_Charges = "order/get_delivery_charges"
//    Call<PromoResponse> getDChargesPCodes(@Field("userId") String userId,
//                                          @Field("token") String token,
//                                          @Field("addressId") String addressId,
//                                          @Field("storeId") String storeId,
//                                          @Field("latitude") String latitude,
//                                          @Field("longitude") String longitude,
//                                          @Field("itemsId") String itemsId);


    
    let Add_Cart = "order/add_cart"
//    Call<AddCartResponse> addToCart(@Field("userId") String userId,
//                                    @Field("token") String token,
//                                    @Field("cart") String cart);



    let Update_Cart = "order/update_cart"
//    Call<AddCartResponse> updateCart(@Field("userId") String userId,
//                                     @Field("token") String token,
//                                     @Field("cart_id") String cart_id,
//                                     @Field("cart") String cart,
//                                     @Field("quantity") String quantity);


    
    let View_Cart = "order/view_cart"
//    Call<CartResponse> getCartResponse(@Field("userId") String userId,
//                                       @Field("token") String token,
//                                       @Field("promo_id") String promo_id);



    let Delete_Cart = "order/delete_cart"
//    Call<AddCartResponse> deleteCart(@Field("userId") String userId,
//                                     @Field("token") String token,
//                                     @Field("cart_id") String cart_id);



    let Get_Favourite_Store = "home/get_fav_store"
//    Call<FavouriteStoreResponse> getFavoriteStores(@Field("userId") String userId,
//                                                   @Field("token") String token,
//                                                   @Field("latitude") String latitude,
//                                                   @Field("longitude") String longitude,
//                                                   @Field("page") String page,
//                                                   @Field("limit") String limit);


    let Get_Favourite_Items = "home/get_fav_items"
//    Call<FavouriteFoodResponse> getFavoriteFood(@Field("userId") String userId,
//                                                @Field("token") String token,
//                                                @Field("page") String page,
//                                                @Field("limit") String limit);


    let Delete_Favourite_Item = "home/del_fav_item"
//    Call<FavouriteFoodResponse> deleteFavoriteFood(@Field("userId") String userId,
//                                                   @Field("token") String token,
//                                                   @Field("itemId") String itemId);


    let Add_Favourite_Item = "home/add_fav_item"
//    Call<FavouriteFoodResponse> addFavoriteFood(@Field("userId") String userId,
//                                                @Field("token") String token,
//                                                @Field("itemId") String itemId);

    
    let Get_Promo = "order/get_promo"
//    Call<FavouriteFoodResponse> getPromoCode(@Field("userId") String userId,
//                                             @Field("token") String token,
//                                             @Field("storeId") String storeId);


    
    let Add_Favourite_Store = "home/add_fav_store"
//    Call<ProfileResponse> addStoreToFav(@Field("userId") String userId,
//                                        @Field("token") String token,
//                                        @Field("storeId") String storeId);


    
    let Delete_Favourite_Store = "home/del_fav_store"
//    Call<ProfileResponse> removeStoreFromFav(@Field("userId") String userId,
//                                             @Field("token") String token,
//                                             @Field("storeId") String storeId);



    

