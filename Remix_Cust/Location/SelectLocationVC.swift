//
//  SelectLocationVC.swift
//  Remix_Cust
//
//  Created by Yogender Saini on 03/06/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit
import CoreLocation

import UIKit
//import GoogleMaps
//import GooglePlaces
import IQKeyboardManagerSwift
import Toast_Swift
import CoreLocation
import Alamofire
import MapKit
import AVFoundation
import GoogleMaps




protocol locationManagerProtocol {
    func returnCurrentLocation(newUserLocation : CLLocation)
}

class SelectLocationVC: UIViewController {
    
    @IBOutlet weak var lbl_CurrentAdd : UILabel!
    @IBOutlet weak var lbl_AddType : UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    var user_Current_Location = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCurrentLocation()
    }
    
    //fatching currnet location
    @objc func getCurrentLocation() {
        LocationManager.shared.getCurrentLocation(vcc: self)
        LocationManager.shared.delegate = self
        
    }
    
    @IBAction func btnAction_back(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    static func getInstance() -> SelectLocationVC {
        let storyboard = UIStoryboard(name: "SelectLocation",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SelectLocationVC") as! SelectLocationVC
    }
    
    // setup map location
    func setMap(userLocation: CLLocation) {
        let markerForCurrentLocation = GMSMarker()
        let clLocationCoOrdinate = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(userLocation.coordinate.latitude ), longitude: CLLocationDegrees(userLocation.coordinate.longitude), zoom: Float(12))
        
        markerForCurrentLocation.position = clLocationCoOrdinate
        markerForCurrentLocation.title = "HOME"
        
        markerForCurrentLocation.iconView = self.setBackGroundimgFrame()
        markerForCurrentLocation.map = self.mapView
        
        self.mapView.mapType = .normal
        self.mapView.animate(to: camera)
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = false
        //        self.mapView.delegate = self
        
    }
    
    func setBackGroundimgFrame() -> UIView? {
        let markerBgVw = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 60))
        markerBgVw.layer.backgroundColor = UIColor.clear.cgColor
        markerBgVw.layer.cornerRadius = 0
        markerBgVw.clipsToBounds = true
        let markerImgVw = UIImageView(frame: CGRect(x: 20, y: 10, width: 60, height: 30))
        markerBgVw.addSubview(markerImgVw)
        markerImgVw.image = UIImage(named: "car_side")
        return markerBgVw
    }
    
    @IBAction func btnAction_Confirm_Address(_ sender : UIButton) {
        //        self.navigationController?.popViewController(animated: true)
        
        self.lbl_CurrentAdd.text = self.lbl_CurrentAdd.text?.trimmingCharacters(in: .whitespaces)
        self.lbl_AddType.text = self.lbl_AddType.text?.trimmingCharacters(in: .whitespaces)
        
        if self.lbl_AddType.hasText {
        } else {
            //            self.view.makeToast ((L102Language.AMLocalizedString(key: "please_enter_Phone, value:")), duration: 2.0, position: .bottom, style: style)
            self.view.makeToast ("Please enter address type")
            return
        }

        if self.lbl_CurrentAdd.text == "" {
            //            self.view.makeToast ((L102Language.AMLocalizedString(key: "please_enter_Phone, value:")), duration: 2.0, position: .bottom, style: style)
            self.view.makeToast ("Address can't be blank")
            return
        }
        
        self.apiCalling_Add_Address()

    }
    
    
}
extension SelectLocationVC {
 
    
    func apiCalling_Add_Address(){

        let params = ["userId" : AppHelper.getUserID(), "token": AppHelper.getUserToken(), "address" : lbl_CurrentAdd.text!, "type" : lbl_AddType.text!, "lat" : user_Current_Location.coordinate.latitude, "long" : self.user_Current_Location.coordinate.longitude] as [String : Any]
                
        AppHelper.apiCallingForDict(apiName: Add_Address, param: params, viewCont: self, successDict: { (successDict, resultDict) in
            print(successDict)
            
            self.navigationController?.popViewController(animated: true)

                        
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
                UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
}
extension SelectLocationVC : locationManagerProtocol {
    
    // getting current location from delegate.
    func returnCurrentLocation(newUserLocation userLocation: CLLocation) {
        print("Return location")
        
//        print(userLocation.coordinate.longitude)
//        print(userLocation.coordinate.latitude)
//        print(LocationManager.shared.currentAddress)
        self.user_Current_Location = userLocation
        self.lbl_CurrentAdd.text = LocationManager.shared.currentAddress
        self.setMap(userLocation: userLocation)
        
        }
}


